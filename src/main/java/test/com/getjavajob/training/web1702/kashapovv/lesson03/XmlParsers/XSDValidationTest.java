package test.com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.CompaniesCatalogue;
import com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers.XSDValidation;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 11.03.2017.
 */
public class XSDValidationTest {
    @Test
    public void validate() throws Exception {
        InputStream xsdStream = CompaniesCatalogue.class.getResourceAsStream("Organizations.xsd");
        InputStream xmlStream = XSDValidationTest.class.getResourceAsStream("OrganizationsTest.xml");
        assertTrue(XSDValidation.validate(xsdStream, xmlStream));
    }

}