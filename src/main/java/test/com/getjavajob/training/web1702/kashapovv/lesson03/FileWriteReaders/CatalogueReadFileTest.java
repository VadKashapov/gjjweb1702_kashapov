package test.com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders;

import com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders.CatalogueReadFile;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import org.junit.Test;
import test.com.getjavajob.training.web1702.kashapovv.lesson03.CompaniesCatalogueTest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 12.03.2017.
 */
public class CatalogueReadFileTest {
    @Test
    public void readFile() throws Exception {
        String org1 = "orgName1 OOO lastNameCeo1 firstNameCeo1 middleNameCeo1 5000000 none lastNameSharer1 40 " +
                "lastNameSharer2 60";

        String dir = CompaniesCatalogueTest.class.getResource("").getPath();
        File orgFile = new File(dir, "orgTest.txt");
        BufferedWriter bufWriter = new BufferedWriter(new FileWriter(orgFile));
        bufWriter.write(org1);
        bufWriter.close();

        Organization org = (Organization) CatalogueReadFile.readFile(orgFile).toArray()[0];

        assertEquals("orgName1", org.getName());
        assertEquals("firstNameCeo1", org.getOrganizationCeo().getFirstName());
        assertEquals("middleNameCeo1", org.getOrganizationCeo().getMiddleName());
        assertEquals("lastNameCeo1", org.getOrganizationCeo().getLastName());
        assertEquals(null, org.getParentOrganization());
        assertEquals("lastNameSharer1", org.getShareholders().get(0).getPerson().getLastName());
        assertEquals(40, org.getShareholders().get(0).getSharesProportion());
        assertEquals("lastNameSharer2", org.getShareholders().get(1).getPerson().getLastName());
        assertEquals(60, org.getShareholders().get(1).getSharesProportion());
    }

}