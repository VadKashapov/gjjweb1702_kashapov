package test.com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders;

import com.getjavajob.training.web1702.kashapovv.lesson03.CompaniesCatalogue;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders.CatalogueReadWriteData.readData;
import static com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders.CatalogueReadWriteData.writeData;
import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 12.03.2017.
 */
public class CatalogueReadWriteDataTest {
    @Test
    public void writeReadDataTest() throws Exception {
        Collection<Organization> orgs = new HashSet<>();
        List<Shareholder> sharers = new ArrayList<>();
        sharers.add(new Shareholder(new Person("lastName1", null, null), 30));
        Person orgCEO = new Person("lastNameCEO", "firstNameCEO", "middleNameCEO");
        Organization org = new Organization("orgName1", OrganizationType.OAO, orgCEO, 1000000, null, sharers);
        orgs.add(org);
        writeData(orgs, CompaniesCatalogue.class.getResource("").getPath());
        File writedData = new File(CompaniesCatalogue.class.getResource("Organizations.dat").getPath());
        Collection<Organization> orgs2 = readData(writedData);
        for (Organization org2 : orgs2) {
            assertEquals(org2.getName(), org.getName());
            assertEquals(org2.getType(), org.getType());
            assertEquals(org2.getStatedCapital(), org.getStatedCapital(), 10e-5);
            assertEquals(org2.getShareholders().get(0), org.getShareholders().get(0));
        }
    }

}