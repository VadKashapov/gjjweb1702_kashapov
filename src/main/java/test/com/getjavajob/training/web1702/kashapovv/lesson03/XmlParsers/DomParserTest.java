package test.com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers.DomParser;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by Вадим on 09.03.2017.
 */
public class DomParserTest {
    @Test
    public void readXml() throws Exception {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("organization", Organization.class);
        xstream.alias("shareholder", Shareholder.class);
        xstream.alias("person", Person.class);
        xstream.aliasAttribute(Organization.class, "name", "name");
        xstream.setMode(XStream.NO_REFERENCES);

        Collection<Organization> orgs = new HashSet<>();
        List<Shareholder> sharers = new ArrayList<>();
        sharers.add(new Shareholder(new Person("lastNameSharer1", null, null), 30));
        Person orgCEO = new Person("lastNameCEO1", "firstNameCEO1", "middleNameCEO1");
        Organization org1 = new Organization("orgName1", OrganizationType.OAO, orgCEO, 1000000, null, sharers);
        Organization org2 = new Organization("orgName2", OrganizationType.IP, orgCEO, 5000000, org1, sharers);
        orgs.add(org1);
        orgs.add(org2);

        String dir = DomParserTest.class.getResource("").getPath();
        BufferedWriter bufWriter = new BufferedWriter(new FileWriter(new File(dir, "OrganizationsTest.xml")));
        bufWriter.write(xstream.toXML(orgs));
        bufWriter.close();

        InputStream istream = DomParserTest.class.getResourceAsStream("OrganizationsTest.xml");
        Collection<Organization> readedOrgs = (Collection<Organization>) new DomParser().readXml(istream);
        Organization readedOrg1 = (Organization) readedOrgs.toArray()[1];
        Organization readedOrg2 = ((Organization) readedOrgs.toArray()[0]);

        assertEquals("orgName1", readedOrg1.getName());
        assertEquals(OrganizationType.OAO, readedOrg1.getType());
        assertEquals("firstNameCEO1", readedOrg1.getOrganizationCeo().getFirstName());
        assertEquals("middleNameCEO1", readedOrg1.getOrganizationCeo().getMiddleName());
        assertEquals("lastNameCEO1", readedOrg1.getOrganizationCeo().getLastName());
        assertEquals(null, readedOrg1.getParentOrganization());
        assertEquals("lastNameSharer1", readedOrg1.getShareholders().get(0).getPerson().getLastName());
        assertEquals(30, readedOrg1.getShareholders().get(0).getSharesProportion());

        assertSame(readedOrg1.getShareholders().get(0).getPerson(),
                readedOrg2.getParentOrganization().getShareholders().get(0).getPerson());
        assertEquals(readedOrg1.getName(),
                readedOrg2.getParentOrganization().getName());
    }
}