package test.com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers.StaxParser;
import com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers.XStreamAnalogue;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 11.03.2017.
 */
public class XStreamAnalogueTest {
    @Test
    public void toXml() throws Exception {
        XStreamAnalogue parser = new XStreamAnalogue();

        Collection<Organization> orgs = new HashSet<>();
        List<Shareholder> sharers = new ArrayList<>();
        sharers.add(new Shareholder(new Person("lastNameSharer1", null, null), 30));
        Person orgCEO = new Person("lastNameCEO1", "firstNameCEO1", "middleNameCEO1");
        Organization org1 = new Organization("orgName1", OrganizationType.OAO, orgCEO, 1000000, null, sharers);
        Organization org2 = new Organization("orgName2", OrganizationType.IP, orgCEO, 5000000, org1, sharers);
        orgs.add(org1);
        orgs.add(org2);

        String xml = parser.toXml(orgs);
        String dir = XStreamAnalogueTest.class.getResource("").getPath();
        BufferedWriter bufWriter = new BufferedWriter(new FileWriter(new File(dir, "OrganizationsTest.xml")));
        bufWriter.write(xml);
        bufWriter.close();
        InputStream is = XStreamAnalogueTest.class.getResourceAsStream("OrganizationsTest.xml");
        Set<Organization> newOrgs = (Set<Organization>) new StaxParser().readXml(is);

        Organization readedOrg1 = (Organization) newOrgs.toArray()[1];
        Organization readedOrg2 = ((Organization) newOrgs.toArray()[0]);

        assertEquals("orgName1", readedOrg1.getName());
        assertEquals(OrganizationType.OAO, readedOrg1.getType());
        assertEquals("firstNameCEO1", readedOrg1.getOrganizationCeo().getFirstName());
        assertEquals("middleNameCEO1", readedOrg1.getOrganizationCeo().getMiddleName());
        assertEquals("lastNameCEO1", readedOrg1.getOrganizationCeo().getLastName());
        assertEquals(null, readedOrg1.getParentOrganization());
        assertEquals("lastNameSharer1", readedOrg1.getShareholders().get(0).getPerson().getLastName());
        assertEquals(30, readedOrg1.getShareholders().get(0).getSharesProportion());

        assertEquals(readedOrg1.getName(),
                readedOrg2.getParentOrganization().getName());
    }

    @Test
    public void fromXml() throws Exception {
        XStreamAnalogue parser = new XStreamAnalogue();

        Collection<Organization> orgs = new HashSet<>();
        List<Shareholder> sharers = new ArrayList<>();
        sharers.add(new Shareholder(new Person("lastNameSharer1", null, null), 30));
        Person orgCEO = new Person("lastNameCEO1", "firstNameCEO1", "middleNameCEO1");
        Organization org1 = new Organization("orgName1", OrganizationType.OAO, orgCEO, 1000000, null, sharers);
        Organization org2 = new Organization("orgName2", OrganizationType.IP, orgCEO, 5000000, org1, sharers);
        orgs.add(org1);
        orgs.add(org2);

        String xml = parser.toXml(orgs);
        String dir = XStreamAnalogueTest.class.getResource("").getPath();
        BufferedWriter bufWriter = new BufferedWriter(new FileWriter(new File(dir, "OrganizationsTest.xml")));
        bufWriter.write(xml);
        bufWriter.close();
        InputStream is = XStreamAnalogueTest.class.getResourceAsStream("OrganizationsTest.xml");
        Set<Organization> newOrgs = (Set<Organization>) parser.fromXml(is);

        Organization readedOrg1 = (Organization) newOrgs.toArray()[1];
        Organization readedOrg2 = ((Organization) newOrgs.toArray()[0]);

        assertEquals("orgName1", readedOrg1.getName());
        assertEquals(OrganizationType.OAO, readedOrg1.getType());
        assertEquals("firstNameCEO1", readedOrg1.getOrganizationCeo().getFirstName());
        assertEquals("middleNameCEO1", readedOrg1.getOrganizationCeo().getMiddleName());
        assertEquals("lastNameCEO1", readedOrg1.getOrganizationCeo().getLastName());
        assertEquals(null, readedOrg1.getParentOrganization());
        assertEquals("lastNameSharer1", readedOrg1.getShareholders().get(0).getPerson().getLastName());
        assertEquals(30, readedOrg1.getShareholders().get(0).getSharesProportion());

        assertEquals(readedOrg1.getName(),
                readedOrg2.getParentOrganization().getName());
    }

}