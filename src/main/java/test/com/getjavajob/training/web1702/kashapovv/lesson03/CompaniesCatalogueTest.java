package test.com.getjavajob.training.web1702.kashapovv.lesson03;

import com.getjavajob.training.web1702.kashapovv.lesson03.CompaniesCatalogue;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 05.03.2017.
 */
public class CompaniesCatalogueTest {
    private static CompaniesCatalogue catalogue;

    @BeforeClass
    public static void setUp() throws Exception {
        catalogue = new CompaniesCatalogue();

        String org1 = "orgName1 OOO lastNameCeo1 firstNameCeo1 middleNameCeo1 5000000 none lastNameSharer1 50 " +
                "lastNameSharer2 50";
        String org2 = "orgName2 OOO lastNameCeo2 firstNameCeo2 middleNameCeo2 1000000 orgName1 lastNameSharer3 10 " +
                "lastNameCeo2 85 lastNameSharer4 5";
        String org3 = "orgName3 OOO lastNameCeo3 firstNameCeo3 middleNameCeo3 4000000 orgName2 lastNameSharer1 10 " +
                "lastNameSharer3 5";

        String orgTest = org1 + '\n' + org2 + '\n' + org3;
        String dir = CompaniesCatalogueTest.class.getResource("").getPath();
        File orgFile = new File(dir, "orgTest.txt");
        BufferedWriter bufWriter = new BufferedWriter(new FileWriter(orgFile));
        bufWriter.write(orgTest);
        bufWriter.close();

        catalogue.readFile(orgFile);
    }

    @Test
    public void orgNameWithMaxSubsidiaries() throws Exception {
        assertEquals("orgName1", catalogue.orgNameWithMaxSubsidiaries());
    }

    @Test
    public void lastNameRichestSharerAllOrg() throws Exception {
        assertEquals("lastNameSharer1", catalogue.lastNameRichestPersonAtAll());
    }
}