package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort;

/**
 * Created by Вадим on 18.03.2017.
 */
public class ConcurrentQuicksort extends QuicksortImpl {
    private static final int THRESHOLD = 100;

    public <T extends Comparable<T>> T[] quickSort(T[] elements) {
        concurrentQuickSort(elements, 0, elements.length - 1);
        return elements;
    }

    private <T extends Comparable<T>> T[] concurrentQuickSort(T[] numbers, int start, int end) {
        int cur = partition(numbers, start, end);
        if (end - start >= THRESHOLD) {
            Thread threadLeft = getSortThread(numbers, start, cur);
            Thread threadRight = getSortThread(numbers, cur + 1, end);
            threadLeft.start();
            threadRight.start();
            try {
                threadLeft.join();
                threadRight.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            quickSort(numbers, start, cur);
            quickSort(numbers, cur + 1, end);
        }
        return numbers;
    }

    private <T extends Comparable<T>> Thread getSortThread(final T[] numbers, final int start, final int end) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                concurrentQuickSort(numbers, start, end);
            }
        });
    }
}
