package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Triangle extends Polygon {
    private static final int CORNER_NUMBER = 3;

    public Triangle(double edgeLength) {
        super(CORNER_NUMBER, edgeLength);
    }

    @Override
    public double getBaseArea() {
        return pow(getEdgeLength(), 2) * sqrt(3) / 4;
    }

    @Override
    public double getInCircleRadius() {
        return getEdgeLength() / (2 * sqrt(3));
    }
}
