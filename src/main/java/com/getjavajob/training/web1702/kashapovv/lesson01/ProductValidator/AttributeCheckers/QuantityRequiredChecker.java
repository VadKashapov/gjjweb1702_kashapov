package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class QuantityRequiredChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) throws ValidationException {
        int quantityRequired = product.getQuantityRequired();
        int quantityAvailable = product.getQuantityAvailable();
        if (isNatural(quantityRequired)) {
            if (isNatural(quantityAvailable)) {
                if (product.getQuantityAvailable() >= quantityRequired) {
                    return true;
                } else {
                    exceptionMessageBuilder.insert(0, "Field: quantityRequired");
                    exceptionMessageBuilder.append("available quantity < required quantity");
                    exceptionMessageBuilder.append(", ").append("quantity available = ")
                            .append(quantityAvailable).append(" must be >= required quantity =")
                            .append(quantityRequired);
                }
            } else {
                exceptionMessageBuilder.insert(0, "Field: quantityAvailable");
            }
        } else {
            exceptionMessageBuilder.insert(0, "Field: quantityRequired");
        }
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}
