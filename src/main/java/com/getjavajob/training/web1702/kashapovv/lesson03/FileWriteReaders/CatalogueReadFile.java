package com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Вадим on 03.03.2017.
 */
public class CatalogueReadFile {
    public static Set<Organization> readFile(File file) {
        Set<Organization> orgs = new HashSet<>();
        OrganizationParser parser = new OrganizationParser(orgs);
        try (BufferedReader bufFileReader = new BufferedReader(new FileReader(file))) {
            while (bufFileReader.ready()) {
                Organization org = parser.parseLine(bufFileReader.readLine());
                if (org != null) {
                    orgs.add(org);
                }
            }
            return orgs;
        } catch (IOException e) {
            System.out.println("Catch error reading file");
            e.printStackTrace();
        }
        return null;
    }
}