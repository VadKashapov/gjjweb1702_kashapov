package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 25.02.2017.
 */
public class SaleSizeCheckerTest {
    private SaleSizeChecker checker;
    private Product product;

    @Before
    public void setUp() throws Exception {
        checker = new SaleSizeChecker();
        product = new Product();
    }

    @Test
    public void validateCorrectSaleSize() throws Exception {
        product.setSaleSize(1000);
        product.setQuantityRequired(10);
        product.setProductPrice(500.00);

        assertTrue(checker.validate(product));
    }

    @Test
    public void validateSaleOverTotal() throws Exception {
        product.setSaleSize(1000);
        product.setQuantityRequired(10);
        product.setProductPrice(300.00);

        assertFalse(checker.validate(product));
    }

    @Test
    public void validateNotPositive() throws Exception {
        product.setSaleSize(-1000);
        product.setQuantityRequired(10);
        product.setProductPrice(450.00);

        assertFalse(checker.validate(product));
    }
}