package com.getjavajob.training.web1702.kashapovv.lesson08;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Вадим on 18.03.2017.
 */
public class MatrixMultiplier {
    public int[][] multiplyMatrix(InputStream imatrix1, InputStream imatrix2) {
        List<int[]> matrix1 = readMatrixSource(imatrix1);
        List<int[]> matrix2 = readMatrixSource(imatrix2);

        int[][] resultMatrix = multiply(matrix1, matrix2);

        try {
            writeResult(resultMatrix);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultMatrix;
    }

    protected int[][] multiply(List<int[]> matrix1, List<int[]> matrix2) {
        if (matrix1.size() == 0 || matrix2.size() == 0 || matrix1.size() != matrix2.get(0).length) {
            throw new IllegalArgumentException("matrix1 size should be NxM and matrix2 size MxK");
        }
        int[][] resultMatrix = new int[matrix1.get(0).length][matrix2.size()];
        for (int i = 0; i < matrix1.get(0).length; i++) {
            for (int j = 0; j < matrix2.size(); j++) {
                resultMatrix[i][j] = matrix1.get(i)[j] * matrix2.get(j)[i];
            }
        }
        return resultMatrix;
    }

    private void writeResult(int[][] resultMatrix) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[0].length; j++) {
                builder.append(resultMatrix[i][j]).append(' ');
            }
            builder.append('\n');
        }
        String dir = this.getClass().getResource("").getPath();
        try (BufferedWriter bufWriter = new BufferedWriter(new FileWriter(new File(dir, "result.txt")))) {
            bufWriter.write(builder.toString());
        }
    }

    private List<int[]> readMatrixSource(InputStream matrix1) {
        List<int[]> matrix = new ArrayList<>();
        int rowLength = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(matrix1))) {
            while (reader.ready()) {
                String[] nums = reader.readLine().split(" ");
                if (rowLength != 0 && rowLength != nums.length) {
                    throw new IllegalArgumentException("Matrix rows size should be equal");
                }
                rowLength = nums.length;
                int[] matrixLine = new int[rowLength];
                for (int i = 0; i < rowLength; i++) {
                    matrixLine[i] = Integer.parseInt(nums[i]);
                }
                matrix.add(matrixLine);
            }
        } catch (IOException | ClassCastException e) {
            e.printStackTrace();
        }
        return matrix;
    }
}
