package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class ClientIdChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) {
        String clientId = product.getClientId();
        return clientId != null && !clientId.isEmpty()
                && clientId.equals(clientId.toUpperCase());
    }
}
