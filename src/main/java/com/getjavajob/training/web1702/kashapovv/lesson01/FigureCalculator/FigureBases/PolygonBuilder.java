package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

/**
 * Created by Вадим on 23.02.2017.
 */
public class PolygonBuilder {
    public static AbstractFigureBase getPolygonFigure(int cornerCount, double edgeLength) {
        if (cornerCount == 3) {
            return new Triangle(edgeLength);
        } else if (cornerCount == 4) {
            return new Square(edgeLength);
        } else if (cornerCount > 4) {
            return new Polygon(cornerCount, edgeLength);
        }
        throw new IllegalArgumentException("Corner number must be positive");
    }
}
