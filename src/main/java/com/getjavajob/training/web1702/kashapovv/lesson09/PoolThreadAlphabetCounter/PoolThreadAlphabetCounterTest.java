package com.getjavajob.training.web1702.kashapovv.lesson09.PoolThreadAlphabetCounter;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 18.03.2017.
 */
public class PoolThreadAlphabetCounterTest {
    @Test
    public void allAlphabetsOnce() throws Exception {
        String source = "АбвгдеЁжзийкЛмноПрстуФхцчшЩъыьэЮя";
        Charset cp1251 = Charset.forName("CP1251");
       List<Map.Entry<Character, Integer>> letterFrequencyEntries =
                new PoolThreadAlphabetCounter().cyrillicLetterFrequency(new ByteArrayInputStream(
                        source.getBytes(cp1251)), cp1251);
        for (Map.Entry<Character, Integer> letterFrequencyEntry : letterFrequencyEntries) {
            assertEquals(1, ((int) letterFrequencyEntry.getValue()));
        }
    }

    @Test
    public void oneAlphabet() throws Exception {
        String source = "ааааАААА";
        Charset cp1251 = Charset.forName("CP1251");
        List<Map.Entry<Character, Integer>> letterFrequencyEntries =
                new PoolThreadAlphabetCounter().cyrillicLetterFrequency(new ByteArrayInputStream(
                        source.getBytes(cp1251)), cp1251);
        assertEquals(8, ((int) letterFrequencyEntries.get(0).getValue()));
    }
}