package com.getjavajob.training.web1702.kashapovv.lesson08;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Вадим on 18.03.2017.
 */
public class MatrixMultiplierTest {
    @Test
    public void multiplySquareMatrix() throws Exception {
        MatrixMultiplier multiplier = new MatrixMultiplier();
        String matrix = "1 2 3\n" +
                "-1 0 3\n" +
                "5 7 8";
        InputStream imatrix1 = new ByteArrayInputStream(matrix.getBytes());
        InputStream imatrix2 = new ByteArrayInputStream(matrix.getBytes());
        assertArrayEquals(new int[][]{
                        {1, -2, 15},
                        {-2, 0, 21},
                        {15, 21, 64}}
                , multiplier.multiplyMatrix(imatrix1, imatrix2));
    }

    @Test
    public void multiplyRectangleMatrix() throws Exception {
        MatrixMultiplier multiplier = new MatrixMultiplier();
        String matrix2 = "1 2 3 4\n" +
                "-1 0 3 -5\n" +
                "5 7 8 0";
        String matrix1 = "2 3 4\n" +
                "0 3 -5\n" +
                "7 8 0\n" +
                "-5 2 1";
        InputStream imatrix1 = new ByteArrayInputStream(matrix1.getBytes());
        InputStream imatrix2 = new ByteArrayInputStream(matrix2.getBytes());
        assertArrayEquals(new int[][]{
                        {2, -3, 20},
                        {0, 0, -35},
                        {21, 24, 0}}
                , multiplier.multiplyMatrix(imatrix1, imatrix2));
    }

    @Test
    public void concurrentMultiplyMatrix() throws Exception {
        ConcurrentMatrixMultiplier multiplier = new ConcurrentMatrixMultiplier();
        String matrix = "1 2 3\n" +
                "-1 0 3\n" +
                "5 7 8";
        InputStream imatrix1 = new ByteArrayInputStream(matrix.getBytes());
        InputStream imatrix2 = new ByteArrayInputStream(matrix.getBytes());
        assertArrayEquals(new int[][]{
                        {1, -2, 15},
                        {-2, 0, 21},
                        {15, 21, 64}}
                , multiplier.multiplyMatrix(imatrix1, imatrix2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ExceptionTestMissedElement() throws Exception {
        MatrixMultiplier multiplier = new MatrixMultiplier();
        String matrix = "1 2 3\n" +
                "-1 0  \n" +
                "5 7 8";
        InputStream imatrix1 = new ByteArrayInputStream(matrix.getBytes());
        InputStream imatrix2 = new ByteArrayInputStream(matrix.getBytes());
        multiplier.multiplyMatrix(imatrix1, imatrix2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ExceptionTestEmptyString() throws Exception {
        MatrixMultiplier multiplier = new MatrixMultiplier();
        String matrix = "";
        InputStream imatrix1 = new ByteArrayInputStream(matrix.getBytes());
        InputStream imatrix2 = new ByteArrayInputStream(matrix.getBytes());
        multiplier.multiplyMatrix(imatrix1, imatrix2);
    }
}