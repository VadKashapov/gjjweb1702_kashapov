package com.getjavajob.training.web1702.kashapovv.lesson10;

import com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort.ConcurrentQuicksort;
import com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort.Quicksort;
import com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort.QuicksortImpl;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Вадим on 18.03.2017.
 */
public class QuicksortTest {
    @Test
    public void forkJoinQuickSort() throws Exception {
        Random random = new Random(200);
        Integer[] numbers = new Integer[1_000_000];
        Quicksort quicksort = new ForkJoinQuicksort();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }
        Integer[] copy = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(copy);
        assertArrayEquals(copy,quicksort.quickSort(numbers));
    }

    @Test
    public void quickSortComp() throws Exception {
        Random random = new Random(2000);
        Integer[] numbers = new Integer[100_000];
        Quicksort quicksort = new QuicksortImpl();
        Quicksort conQuicksort = new ConcurrentQuicksort();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }
        Integer[] copy = Arrays.copyOf(numbers, numbers.length);
        Integer[] copy2 = Arrays.copyOf(numbers, numbers.length);
        ForkJoinQuicksort forkJoinQuicksort = new ForkJoinQuicksort();

        long start = currentTimeMillis();
        quicksort.quickSort(numbers);
        long singleThreadTimeEnd = currentTimeMillis() - start;

        start = currentTimeMillis();
        conQuicksort.quickSort(copy);
        long manyThreadTimeEnd = currentTimeMillis() - start;

        start = currentTimeMillis();
        forkJoinQuicksort.quickSort(copy2);
        long forkJoinTimeEnd = currentTimeMillis() - start;

        System.out.println("SingleThread: " + singleThreadTimeEnd + " ms");
        System.out.println("ManyThread: " + manyThreadTimeEnd + " ms");
        System.out.println("ForkJoin: " + forkJoinTimeEnd + " ms");
    }
}