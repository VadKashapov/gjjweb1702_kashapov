package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerSemaphore;

import java.util.concurrent.Semaphore;

import static com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerSemaphore.Main.Data.value;
import static java.lang.Thread.sleep;

/**
 * Created by Вадим on 22.03.2017.
 */
public class NumConsumer implements Runnable {
    private Semaphore producer;
    private Semaphore sem;

    public NumConsumer(Semaphore sem, Semaphore producer) {
        this.producer = producer;
        this.sem = sem;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sem.acquire();
                System.out.println(value);
                sleep(1000L);
                producer.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
