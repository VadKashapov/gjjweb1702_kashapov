package com.getjavajob.training.web1702.kashapovv.lesson02.Organization;

import com.getjavajob.training.web1702.kashapovv.lesson02.Exceptions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Вадим on 05.03.2017.
 */
public class OrganizationParser {
    private Set<Organization> orgs;

    public OrganizationParser(Set<Organization> orgs) {
        this.orgs = orgs;
    }

    public Organization parseLine(String line) throws OrganizationTypeException, MoneyValueException,
            ParentOrgException, ShareValueException, SharerDataException {
        String[] inputValues = line.split(" ");
        if (inputValues.length < 8) {
            throw new IllegalArgumentException("At least should be not less 8 values in line");
        }
        if (getOrganization(inputValues[0]) != null) {
            throw new IllegalArgumentException("Organization" + inputValues[0] + "already exist");
        }
        OrganizationType orgType = getOrgType(inputValues[1]);
        double statedCapital = getMoneyValue(inputValues[5]);
        Organization parentOrg = checkParentOrg(inputValues[6]);

        List<Shareholder> inputSharers = new ArrayList<>();
        for (int i = 7; i < inputValues.length; i++) {
            String sharerLastName = inputValues[i];
            i++;
            inputSharers.add(new Shareholder(checkPersonExist(new Person(null, null, sharerLastName))
                    , checkShareProcValue(inputValues[i])));
        }

        int sum = 0;
        for (Shareholder sharer : inputSharers) {
            sum += sharer.getSharesProportion();
        }
        if (sum > 100) {
            throw new ShareValueException("Overall share proportion more 100");
        }
        return new Organization(inputValues[0], orgType, checkPersonExist(new Person(inputValues[2], inputValues[3]
                , inputValues[4])), statedCapital, parentOrg, inputSharers);
    }

    private Person checkPersonExist(Person person) {
        for (Organization org : orgs) {
            for (Shareholder sharer : org.getShareholders()) {
                if (sharer.getPerson().equals(person)) {
                    return sharer.getPerson();
                }
            }
        }
        return person;
    }

    private int checkShareProcValue(String inputValue) throws ShareValueException, SharerDataException {
        try {
            int value = Integer.parseInt(inputValue);
            if (value <= 0 || value > 100) {
                throw new ShareValueException("Share proportion must be positive and < 100");
            } else {
                return value;
            }
        } catch (NumberFormatException e) {
            throw new SharerDataException("Sharer data incorrect", e);
        }
    }

    private Organization checkParentOrg(String inputValue) throws ParentOrgException {
        Organization org = getOrganization(inputValue);
        if (!inputValue.equalsIgnoreCase("NONE") && org == null) {
            throw new ParentOrgException("Parent organization not found");
        } else {
            return org;
        }
    }

    private double getMoneyValue(String inputValue) throws MoneyValueException {
        try {
            double value = Double.valueOf(inputValue);
            if (value <= 0) {
                throw new MoneyValueException("Money value must be > 0");
            } else {
                return value;
            }
        } catch (NumberFormatException e) {
            throw new MoneyValueException("Wrong money value (not number)", e);
        }
    }

    private OrganizationType getOrgType(String inputValue) throws OrganizationTypeException {
        try {
            return OrganizationType.valueOf(inputValue.toUpperCase());
        } catch (IllegalArgumentException exc) {
            throw new OrganizationTypeException("Wrong organization type", exc);
        }
    }

    private Organization getOrganization(String orgName) {
        if (orgs == null) {
            return null;
        }
        for (Organization org : orgs) {
            if (org.getName().equals(orgName)) {
                return org;
            }
        }
        return null;
    }
}