package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class EmailChecker extends AbstractProductAttributeChecker {

    @Override
    public boolean validate(Product product) throws ValidationException {
        String clientEmail = product.getClientEmail();
        if (isNotEmpty(clientEmail)) {
            if (clientEmail.matches("[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-\\.]+")) {
                return true;
            } else {
                exceptionMessageBuilder.append("value not matches with Email pattern");
                exceptionMessageBuilder.append(", ").append("value = ").append(clientEmail).append(", " +
                        "\nexpected form: [chars]@[chars].[chars]" +
                        "\nexpected symbols:\nBefore @ sign: [a-zA-Z0-9_.+-]\n" +
                        "After @ sign: [a-zA-Z0-9-]\nAfter dot: [a-zA-Z0-9-.]");
            }
        }
        exceptionMessageBuilder.insert(0,"Field: emailID");
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}
