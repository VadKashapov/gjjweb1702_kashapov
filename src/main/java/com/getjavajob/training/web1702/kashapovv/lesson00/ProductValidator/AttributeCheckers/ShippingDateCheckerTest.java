package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 25.02.2017.
 */
public class ShippingDateCheckerTest {
    private ShippingDateChecker checker;
    private Product product;

    @Before
    public void setUp() throws Exception {
        checker = new ShippingDateChecker();
        product = new Product();
    }

    @Test
    public void validateDateAfterOrderDateOnOneDay() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);

        product.setShippingDate(product.getDateFormat().format(calendar.getTime()));
        assertTrue(checker.validate(product));
    }

    @Test
    public void validateDateBeforeOrderDateOnOneDay() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        product.setShippingDate(product.getDateFormat().format(calendar.getTime()));
        assertFalse(checker.validate(product));
    }
}