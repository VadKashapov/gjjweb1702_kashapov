package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.InputStream;

/**
 * Created by Вадим on 09.03.2017.
 */
public class XStreamParser implements XmlOrganizationParser {

    @Override
    public Object readXml(InputStream inputStream) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("organization", Organization.class);
        xstream.alias("shareholder", Shareholder.class);
        xstream.alias("person", Person.class);
        xstream.aliasAttribute(Organization.class, "name", "name");

        xstream.processAnnotations(Organization.class);
        return xstream.fromXML(inputStream);
    }
}