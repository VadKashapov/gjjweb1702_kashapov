package com.getjavajob.training.web1702.kashapovv.lesson03.Exceptions;

import java.io.IOException;

/**
 * Created by Вадим on 03.03.2017.
 */
public class MoneyValueException extends IOException {
    public MoneyValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoneyValueException(String message) {
        super(message);
    }
}
