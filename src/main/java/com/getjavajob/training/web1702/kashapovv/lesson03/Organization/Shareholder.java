package com.getjavajob.training.web1702.kashapovv.lesson03.Organization;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

/**
 * Created by Вадим on 02.03.2017.
 */

@XStreamAlias("shareholder")
//@XmlAlias("shareholder")
public class Shareholder implements Serializable {
    private Person person;
    private int sharesProportion;

    public Shareholder(){}

    public Shareholder(Person person, int sharesProportion) {
        this.person = person;
        this.sharesProportion = sharesProportion;
    }

    public int getSharesProportion() {
        return sharesProportion;
    }

    public Person getPerson() {
        return person;
    }

    @Override
    public String toString() {
        return "Shareholder: " + person.getLastName() + ' ' + person.getFirstName() + ' ' + person.getMiddleName() +
                " sharesProp=" + sharesProportion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shareholder that = (Shareholder) o;
        if (getSharesProportion() != that.getSharesProportion()) {
            return false;
        }
        return getPerson().equals(that.getPerson());

    }

    @Override
    public int hashCode() {
        int result = getPerson().hashCode();
        result = 31 * result + getSharesProportion();
        return result;
    }
}