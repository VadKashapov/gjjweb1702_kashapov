package com.getjavajob.training.web1702.kashapovv.lesson02.Organization;

import java.io.Serializable;

/**
 * Created by Вадим on 02.03.2017.
 */
public class Shareholder implements Serializable {
    private Person person;
    private int sharesProportion;

    public Shareholder(Person person, int sharesProportion) {
        this.person = person;
        this.sharesProportion = sharesProportion;
    }

    public int getSharesProportion() {
        return sharesProportion;
    }

    public Person getPerson() {
        return person;
    }
}