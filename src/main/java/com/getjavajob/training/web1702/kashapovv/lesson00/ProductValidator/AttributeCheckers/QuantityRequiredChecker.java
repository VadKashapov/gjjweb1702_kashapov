package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class QuantityRequiredChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) {
        int quantityRequired = product.getQuantityRequired();
        return quantityRequired > 0 && product.getQuantityAvailable() >= quantityRequired;
    }
}
