package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Вадим on 23.02.2017.
 */
public class EmailCheckerTest {
    private EmailChecker checker;
    private Product product;

    @Before
    public void setUp() throws Exception {
        checker = new EmailChecker();
        product = new Product();
    }

    @Test
    public void validateEmailWithCommonWrite() throws Exception {
        product.setClientEmail("LastName93@mail.com");
        assertEquals(true, checker.validate(product));
    }

    @Test
    public void validateEmailNumsBeforeAndAfterDog() throws Exception {
        product.setClientEmail("1991@1991.com");
        assertEquals(true, checker.validate(product));
    }

    @Test
    public void validateEmailWithDots() throws Exception {
        product.setClientEmail("v.f.kashapov@mail.com");
        assertEquals(true, checker.validate(product));
    }

    @Test
    public void validateEmailWithoutWordBeforeDog() throws Exception {
        product.setClientEmail("@mail.com");
        assertEquals(false, checker.validate(product));
    }

    @Test
    public void validateEmailWithoutDogSign() throws Exception {
        product.setClientEmail("MailNamemail.com");
        assertEquals(false, checker.validate(product));
    }
}