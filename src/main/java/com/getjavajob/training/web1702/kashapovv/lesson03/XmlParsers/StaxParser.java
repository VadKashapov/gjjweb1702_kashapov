package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Вадим on 10.03.2017.
 */
public class StaxParser implements XmlOrganizationParser {
    private Map<Person, Person> persons = new HashMap();
    private Set<Organization> orgs = new HashSet();

    @Override
    public Object readXml(InputStream istream) throws XMLStreamException {
        Deque<Organization> stack = new ArrayDeque<>();
        Organization org = null;
        Organization parentOrg = null;
        OrganizationType type = null;
        Person orgCeo = null;
        Person person = null;
        double statedCapital = 0;
        List<Shareholder> sharers = null;
        int sharesProportion = 0;
        String firstName = null;
        String middleName = null;
        String lastName = null;

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(istream);
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "organization":
                            org = new Organization();
                            org.setName(reader.getAttributeValue(0));
                            break;
                        case "type":
                            reader.next();
                            org.setType(OrganizationType.valueOf(reader.getText().trim()));
                            break;
                        case "firstName":
                            reader.next();
                            firstName = reader.getText().trim();
                            break;
                        case "middleName":
                            reader.next();
                            middleName = reader.getText().trim();
                            break;
                        case "lastName":
                            reader.next();
                            lastName = reader.getText().trim();
                            break;
                        case "statedCapital":
                            reader.next();
                            org.setStatedCapital(Double.valueOf(reader.getText().trim()));
                            break;
                        case "parentOrganization":
                            stack.add(org);
                            org = new Organization();
                            org.setName(reader.getAttributeValue(0));
                            break;
                        case "shareholders":
                            sharers = new ArrayList<>();
                            break;
                        case "sharesProportion":
                            reader.next();
                            sharesProportion = Integer.parseInt(reader.getText().trim());
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "organization":
                            orgs.add(org);
                            break;
                        case "organizationCeo":
                            org.setOrganizationCeo(checkPersonExist(new Person(lastName, firstName, middleName)));
                            firstName = null;
                            middleName = null;
                            lastName = null;
                            break;
                        case "person":
                            person = checkPersonExist(new Person(lastName, firstName, middleName));
                            firstName = null;
                            middleName = null;
                            lastName = null;
                            break;
                        case "shareholder":
                            sharers.add(new Shareholder(person, sharesProportion));
                            sharesProportion = 0;
                            person = null;
                            break;
                        case "shareholders":
                            org.setShareholders(sharers);
                            break;
                        case "parentOrganization":
                            Organization daughterOrg = stack.pop();
                            daughterOrg.setParentOrganization(org);
                            org = daughterOrg;
                            break;
                    }
                    break;
            }
        }
        return orgs;
    }

    private Person checkPersonExist(Person person) {
        if (persons.containsKey(person)) {
            return persons.get(person);
        } else {
            persons.put(person, person);
            return person;
        }
    }
}
