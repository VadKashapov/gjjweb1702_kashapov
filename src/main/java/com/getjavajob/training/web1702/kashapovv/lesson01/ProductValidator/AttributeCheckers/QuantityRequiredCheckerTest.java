package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 25.02.2017.
 */
public class QuantityRequiredCheckerTest {
    private Product product;
    private QuantityRequiredChecker checker;

    @Before
    public void setUp() throws Exception {
        checker = new QuantityRequiredChecker();
        product = new Product();
    }

    @Test
    public void validateCorrectQuantityRequiemAvailable() throws Exception {
        product.setQuantityRequired(10);
        product.setQuantityAvailable(20);

        assertTrue(checker.validate(product));
    }

    @Test
    public void validateQuantityOverAvailableTest() throws Exception {
        product.setQuantityRequired(30);
        product.setQuantityAvailable(20);

        assertFalse(checker.validate(product));
    }

    @Test
    public void validateQuantityNegativeTest() throws Exception {
        product.setQuantityRequired(-30);
        product.setQuantityAvailable(20);

        assertFalse(checker.validate(product));
    }
}