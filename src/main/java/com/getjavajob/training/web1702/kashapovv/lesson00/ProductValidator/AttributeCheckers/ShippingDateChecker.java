package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

import java.util.Date;

/**
 * Created by Вадим on 23.02.2017.
 */
public class ShippingDateChecker extends AbstractProductAttributeChecker {

    @Override
    public boolean validate(Product product) {
        return product.getShippingDate().after(new Date());
    }
}
