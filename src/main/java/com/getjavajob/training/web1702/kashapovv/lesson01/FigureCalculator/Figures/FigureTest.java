package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.Circle;
import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.Polygon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Вадим on 01.03.2017.
 */
public class FigureTest {
    @Test
    public void hashCodeTest() throws Exception {
        assertEquals(new Cone(15, 5).hashCode(), new Cone(15, 5).hashCode());
        assertNotEquals(new Cone(15, 5).hashCode(), new Cone(10, 3).hashCode());
    }

    @Test
    public void equalsTest() throws Exception {
        assertEquals(true, new Cone(15, 5).equals(new Cone(15, 5)));
        assertEquals(true, new Pyramid(4, 5, 5).equals(new Pyramid(4, 5, 5)));
        assertEquals(true, new Prism(4, 5, 5).equals(new Prism(4, 5, 5)));
    }

    @Test
    public void pyramidCloneTest() throws Exception {
        Pyramid pyramid = new Pyramid(4, 5, 5);
        Pyramid pyrClone = (Pyramid) pyramid.clone();
        pyrClone.setHeight(10);
        Polygon pyrBaseClone = (Polygon) pyrClone.figureBase;
        pyrBaseClone.setCornerCount(5);
        pyrBaseClone.setEdgeLength(10);
        assertEquals(4, ((Polygon) pyramid.figureBase).getCornerCount());
        assertEquals(5, ((Polygon) pyramid.figureBase).getEdgeLength(), 10e-5);
        assertEquals(5, pyramid.getHeight(), 10e-5);
    }

    @Test
    public void coneCloneTest() throws Exception {
        Cone cone = new Cone(15, 5);
        Cone coneClone = (Cone) cone.clone();
        coneClone.setHeight(6);
        Circle coneCloneBase = ((Circle) coneClone.figureBase);
        coneCloneBase.setRadius(10);
        assertEquals(5, cone.getHeight(), 10e-5);
        assertEquals(15, ((Circle) cone.figureBase).getRadius(), 10e-5);
    }

    @Test
    public void toStringTest() throws Exception {
        assertEquals("Figure: Cone\nHeight: 5.0 and base :\nCircle radius: 15.0"
                , new Cone(15, 5).toString());
        assertEquals("Figure: Pyramid\nHeight: 5.0 and base :\nSquare has 5.0 edge length"
                , new Pyramid(4, 5, 5).toString());
        assertEquals("Figure: Prism\nHeight: 5.0 and base :\nPolygon has: 5 corners, 5.0 edge length"
                , new Prism(5, 5, 5).toString());
        assertEquals("Figure: Cylinder\nHeight: 5.0 and base :\nCircle radius: 15.0"
                , new Cylinder(15, 5).toString());
    }
}