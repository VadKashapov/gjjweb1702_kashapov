package com.getjavajob.training.web1702.kashapovv.lesson02;

import com.getjavajob.training.web1702.kashapovv.lesson02.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson02.Organization.OrganizationParser;
import com.getjavajob.training.web1702.kashapovv.lesson02.Organization.Shareholder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Вадим on 02.03.2017.
 */
public class CompaniesCatalogue {
    public final static String FILE_NAME = "Organizations.dat";

    private Set<Organization> orgs;

    public CompaniesCatalogue() {
        URL baseUrl = this.getClass().getResource(FILE_NAME);
        if (baseUrl != null) {
            Set<Organization> orgs = CatalogueReadWriteData.readData(new File(baseUrl.getPath()));
            if (orgs != null) {
                this.orgs = orgs;
            } else {
                this.orgs = new HashSet<>();
            }
        } else {
            this.orgs = new HashSet<>();
        }
    }

    public static void main(String[] args) {
        CompaniesCatalogue catalogue = new CompaniesCatalogue();
        if (args[0] != null) {
            String filePath = CompaniesCatalogue.class.getResource(args[0]).getPath();
            catalogue.readFile(new File(filePath));
        }
        //     catalogue.readConsole();
        System.out.println("Most richest person: " + catalogue.lastNameRichestPersonAtAll());
        System.out.println("Organization with max subsidiaries: " + catalogue.orgNameWithMaxSubsidiaries());
        catalogue.saveData();
    }

    public void readFile(File file) {
        Set<Organization> newOrgs = CatalogueReadFile.readFile(file);
        if (newOrgs != null) {
            orgs.addAll(newOrgs);
        }
    }

    private void saveData() {
        CatalogueReadWriteData.writeData(orgs);
    }

    public void readConsole() {
        System.out.println("Input organization info in program appropriate format (Empty line for ending):");
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            for (String line = bufReader.readLine(); !line.equals(""); line = bufReader.readLine()) {
                orgs.add(new OrganizationParser(orgs).parseLine(line));
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public String orgNameWithMaxSubsidiaries() {
        Map<Organization, Integer> orgMap = new HashMap<>();

        for (Organization org : orgs) {
            orgMap.put(org, 0);
        }
        for (Organization org : orgs) {
            while (org.getParentOrganization() != null) {
                orgMap.put(org.getParentOrganization(), orgMap.get(org.getParentOrganization()) + 1);
                org = org.getParentOrganization();
            }
        }

        Map.Entry<Organization, Integer> maxEntry = null;

        for (Map.Entry<Organization, Integer> entry : orgMap.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry != null ? maxEntry.getKey().getName() : null;
    }

    public String lastNameRichestPersonAtAll() {
        Map<Shareholder, Double> sharerWealthMap = new HashMap<>();

        for (Organization org : orgs) {
            for (Shareholder sharer : org.getShareholders()) {
                Double sharerMoney = org.getStatedCapital() * sharer.getSharesProportion() / 100.00;
                Double personMoneyOverall = sharerWealthMap.put(sharer, sharerMoney);
                if (personMoneyOverall != null) {
                    sharerWealthMap.put(sharer, sharerMoney + personMoneyOverall);
                }
            }
        }

        Map.Entry<Shareholder, Double> maxEntry = null;

        for (Map.Entry<Shareholder, Double> entry : sharerWealthMap.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry != null ? maxEntry.getKey().getPerson().getLastName() : null;
    }
}