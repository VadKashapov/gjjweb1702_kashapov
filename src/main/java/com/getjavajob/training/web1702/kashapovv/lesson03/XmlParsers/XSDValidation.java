package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Вадим on 11.03.2017.
 */
public class XSDValidation {
    public static boolean validate(InputStream xsdStream, InputStream xmlStream) throws SAXException, IOException {
        List<Source> schemas = new ArrayList<>();
        schemas.add(new StreamSource(xsdStream));
        Source xmlFile = new StreamSource(xmlStream);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source[] schemaSources = new Source[schemas.size()];
        schemas.toArray(schemaSources);
        Schema schema = schemaFactory.newSchema(schemaSources);
        Validator validator = schema.newValidator();
        try {
            validator.validate(xmlFile);
            return true;
        } catch (SAXException e) {
            System.out.println("Document not valid");
            System.out.println(e.getLocalizedMessage());
            return false;
        }
    }
}
