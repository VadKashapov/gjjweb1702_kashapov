package com.getjavajob.training.web1702.kashapovv.lesson03.Organization;

/**
 * Created by Вадим on 02.03.2017.
 */
public enum OrganizationType {
    OOO, OAO, ZAO, IP
}