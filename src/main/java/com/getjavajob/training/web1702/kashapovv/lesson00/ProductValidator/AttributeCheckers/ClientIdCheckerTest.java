package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 25.02.2017.
 */
public class ClientIdCheckerTest {
    private Product product;
    private ClientIdChecker checker;

    @Before
    public void setUp() throws Exception {
        checker = new ClientIdChecker();
        product = new Product();
    }

    @Test
    public void validateCorrectClientId() throws Exception {
        product.setClientId("UPPERCASE");
        assertEquals(true, checker.validate(product));
    }

    @Test
    public void validateClientIdWithLowerCaseChar() throws Exception {
        product.setClientId("UPPeRCASE");
        assertEquals(false, checker.validate(product));
    }

    @Test
    public void validateEmptyClientId() throws Exception {
        product.setClientId("");
        assertEquals(false, checker.validate(product));
    }

    @Test
    public void validateNullClientId() throws Exception {
        product.setClientId(null);
        assertEquals(false, checker.validate(product));
    }
}