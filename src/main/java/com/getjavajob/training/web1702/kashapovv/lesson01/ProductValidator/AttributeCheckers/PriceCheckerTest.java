package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by Вадим on 25.02.2017.
 */
public class PriceCheckerTest {
    private Product product;
    private PriceChecker checker;

    @Before
    public void setUp() throws Exception {
        checker = new PriceChecker();
        product = new Product();
    }

    @Test
    public void validateCorrectProductPrice() throws Exception {
        product.setProductPrice(1999.99);
        assertTrue(checker.validate(product));
    }

    @Test
    public void validateZeroProductPrice() throws Exception {
        product.setProductPrice(0);
        assertFalse(checker.validate(product));
    }

    @Test
    public void validateIncorrectPrice() throws Exception {
        product.setProductPrice(-1000.99);
        assertFalse(checker.validate(product));
    }
}