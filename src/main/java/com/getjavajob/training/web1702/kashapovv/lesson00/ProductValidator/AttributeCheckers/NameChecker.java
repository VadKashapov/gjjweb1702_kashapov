package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class NameChecker extends AbstractProductAttributeChecker {
    public boolean validate(Product product) {
        String productName = product.getProductName();
        return productName != null && !productName.isEmpty();
    }
}
