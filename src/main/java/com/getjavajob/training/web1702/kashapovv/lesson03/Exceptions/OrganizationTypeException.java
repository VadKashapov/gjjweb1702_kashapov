package com.getjavajob.training.web1702.kashapovv.lesson03.Exceptions;

import java.io.IOException;

/**
 * Created by Вадим on 02.03.2017.
 */
public class OrganizationTypeException extends IOException {

    public OrganizationTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
