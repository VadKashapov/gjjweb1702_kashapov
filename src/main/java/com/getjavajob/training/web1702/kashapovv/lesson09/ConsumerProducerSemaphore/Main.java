package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerSemaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Created by Вадим on 23.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphoreEvenThread = new Semaphore(0);
        Semaphore semaphoreOddThread = new Semaphore(0);
        Semaphore semaphoreProducer = new Semaphore(1);
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(new NumConsumer(semaphoreEvenThread,semaphoreProducer));
        service.execute(new NumConsumer(semaphoreOddThread,semaphoreProducer));
        service.execute(new NumProducer(semaphoreEvenThread,semaphoreOddThread,semaphoreProducer));
    }

    static class Data {
        public static volatile int value;
    }
}
