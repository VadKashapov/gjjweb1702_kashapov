package com.getjavajob.training.web1702.kashapovv.lesson02.Organization;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Вадим on 02.03.2017.
 */
public class Organization implements Serializable {
    private static final long serialVersionUID = 9165250388362785721L;

    private String name;
    private OrganizationType type;
    private Person organizationCeo;
    private double statedCapital;
    private Organization parentOrganization;
    private List<Shareholder> shareholders;

    public Organization(String name, OrganizationType type, Person organizationCeo, double statedCapital
            , Organization parentOrganization, List<Shareholder> shareholders) {
        this.name = name;
        this.type = type;
        this.organizationCeo = organizationCeo;
        this.statedCapital = statedCapital;
        this.parentOrganization = parentOrganization;
        this.shareholders = shareholders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationType getType() {
        return type;
    }

    public void setType(OrganizationType type) {
        this.type = type;
    }

    public Person getOrganizationCeo() {
        return organizationCeo;
    }

    public void setOrganizationCeo(Person organizationCeo) {
        this.organizationCeo = organizationCeo;
    }

    public double getStatedCapital() {
        return statedCapital;
    }

    public void setStatedCapital(double statedCapital) {
        this.statedCapital = statedCapital;
    }

    public Organization getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(Organization parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public List<Shareholder> getShareholders() {
        return shareholders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Organization that = (Organization) o;

        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
