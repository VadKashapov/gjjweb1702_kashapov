package com.getjavajob.training.web1702.kashapovv.lesson03.Organization;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Вадим on 02.03.2017.
 */

//@XmlAlias("organization")
public class Organization implements Serializable {

    @XmlTransient
    private transient static final long serialVersionUID = 9165250388362785721L;

    @XmlAlias("name")
    @XmlAttribute
    private String name;
    private OrganizationType type;
    private Person organizationCeo;
    private double statedCapital;
    private Organization parentOrganization;
    private List<Shareholder> shareholders;

    public Organization() {
    }

    public Organization(String name, OrganizationType type, Person organizationCeo, double statedCapital
            , Organization parentOrganization, List<Shareholder> shareholders) {
        this.name = name;
        this.type = type;
        this.organizationCeo = organizationCeo;
        this.statedCapital = statedCapital;
        this.parentOrganization = parentOrganization;
        this.shareholders = shareholders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationType getType() {
        return type;
    }

    public void setType(OrganizationType type) {
        this.type = type;
    }

    public Person getOrganizationCeo() {
        return organizationCeo;
    }

    public void setOrganizationCeo(Person organizationCeo) {
        this.organizationCeo = organizationCeo;
    }

    public double getStatedCapital() {
        return statedCapital;
    }

    public void setStatedCapital(double statedCapital) {
        this.statedCapital = statedCapital;
    }

    public Organization getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(Organization parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public List<Shareholder> getShareholders() {
        return shareholders;
    }

    public void setShareholders(List<Shareholder> shareholders) {
        this.shareholders = shareholders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Organization that = (Organization) o;
        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public String toString() {
        return "Organization: " + '\'' + name + '\'' +
                ", type=" + type +
                ", organizationCeo=" + organizationCeo.toString() +
                ", statedCapital=" + statedCapital +
                ", parentOrganization=" + (parentOrganization == null ? "none" : parentOrganization.getName()) +
                ", shareholders=" + Arrays.toString(shareholders.toArray()) + "\n";
    }
}
