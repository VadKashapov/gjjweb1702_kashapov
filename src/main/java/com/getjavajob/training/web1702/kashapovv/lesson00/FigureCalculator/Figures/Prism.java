package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.Figures;

import static com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases.PolygonBuilder.getPolygonFigure;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Prism extends AbstractRightFigure {

    public Prism(int cornerCount, double edgeLength, double height) {
        super(getPolygonFigure(cornerCount, edgeLength), height);
    }
}
