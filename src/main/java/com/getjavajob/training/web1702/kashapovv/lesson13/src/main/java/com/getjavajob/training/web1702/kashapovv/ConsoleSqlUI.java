package com.getjavajob.training.web1702.kashapovv.lesson13.src.main.java.com.getjavajob.training.web1702.kashapovv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static java.lang.System.out;

/**
 * Created by Вадим on 02.05.2017.
 */
public class ConsoleSqlUI {
    private SqlRunner runner;

    public static void main(String[] args) {
        ConsoleSqlUI client = new ConsoleSqlUI();
        client.startSession();
    }

    public void startSession() {
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            Properties prop = new Properties();
            prop.load(this.getClass().getClassLoader().getResourceAsStream("lesson13.properties"));
            String url = prop.getProperty("database.url");
            String username = prop.getProperty("database.username");
            String password = prop.getProperty("database.password");
            runner = new SqlRunner();

            out.println("Trying to connect " + url);
            runner.createDatabaseConnection(url, username, password);
            out.println("Connection established");
            out.println("Input query or \"quit\":");

            String input;
            while (!(input = consoleReader.readLine()).equalsIgnoreCase("quit")) {
                printValues(runner.executeQuery(input));
            }
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                runner.closeConnection();
                out.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void printValues(List<String[]> rows) {
        int columnNumber = 0;
        if (!rows.isEmpty()) {
            columnNumber = rows.get(0).length;
        } else {
            out.println("Returned empty set");
            return;
        }
        int[] maxColumnLength = new int[columnNumber];
        for (int j = 0; j < rows.size(); j++) {
            for (int i = 0; i < columnNumber; i++) {
                int length = rows.get(j)[i].length();
                if (maxColumnLength[i] < length) {
                    maxColumnLength[i] = length;
                }
            }
        }

        StringBuilder builder = new StringBuilder();
        int lineLength = 0;
        for (int i = 0; i < maxColumnLength.length; i++) {
            lineLength += maxColumnLength[i] + 1;
        }
        lineLength--;

        char[] line = new char[lineLength];
        Arrays.fill(line, '-');
        builder.append('+').append(line).append('+').append('\n');

        boolean isLabel = true;
        for (String[] row : rows) {
            for (int i = 0; i < row.length; i++) {
                char[] spaces = new char[maxColumnLength[i] - row[i].length()];
                Arrays.fill(spaces, ' ');
                builder.append('|').append(row[i]).append(spaces);
            }
            builder.append('|').append('\n');
            if (isLabel) {
                isLabel = false;
                builder.append('+').append(line).append('+').append('\n');
            }
        }
        builder.append('+').append(line).append('+').append('\n');
        out.println(builder.toString());
    }
}
