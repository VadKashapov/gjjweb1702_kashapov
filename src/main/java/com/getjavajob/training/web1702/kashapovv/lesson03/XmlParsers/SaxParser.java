package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.CompaniesCatalogue;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Вадим on 10.03.2017.
 */
public class SaxParser implements XmlOrganizationParser {
    public static void main(String[] args) throws Exception {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(CompaniesCatalogue.class.getResourceAsStream("Organizations.xml"), handler);
        System.out.println(Arrays.toString(handler.orgs.toArray()));
    }

    @Override
    public Object readXml(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(inputStream, handler);
        return handler.orgs;
    }

    static class SAXHandler extends DefaultHandler {
        private Deque<Organization> stack = new ArrayDeque<>();
        private Set<Organization> orgs = new HashSet<>();
        private Map<Person, Person> persons = new HashMap<>();
        private Organization org = null;
        private String lastName = null;
        private String middleName = null;
        private String firstName = null;
        private Person person = null;
        private List<Shareholder> sharers = new ArrayList<>();
        private int sharesProportion = 0;
        private String content = null;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            switch (qName) {
                case "organization":
                    org = new Organization();
                    org.setName(attributes.getValue("name"));
                    break;
                case "parentOrganization":
                    stack.add(org);
                    org = new Organization();
                    org.setName(attributes.getValue("name"));
                    break;
                case "shareholders":
                    sharers = new ArrayList<>();
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch (qName) {
                case "organization":
                    orgs.add(org);
                    break;
                case "type":
                    org.setType(OrganizationType.valueOf(content));
                    break;
                case "lastName":
                    lastName = content;
                    break;
                case "middleName":
                    middleName = content;
                    break;
                case "firstName":
                    firstName = content;
                    break;
                case "person":
                    person = checkPersonExist(new Person(lastName, firstName, middleName));
                    firstName = null;
                    middleName = null;
                    lastName = null;
                    break;
                case "organizationCeo":
                    org.setOrganizationCeo(new Person(lastName, firstName, middleName));
                    firstName = null;
                    middleName = null;
                    lastName = null;
                    break;
                case "statedCapital":
                    org.setStatedCapital(Double.parseDouble(content));
                    break;
                case "sharesProportion":
                    sharesProportion = Integer.parseInt(content);
                    break;
                case "shareholder":
                    sharers.add(new Shareholder(person, sharesProportion));
                    break;
                case "shareholders":
                    org.setShareholders(sharers);
                    break;
                case "parentOrganization":
                    Organization tempOrg = stack.pop();
                    tempOrg.setParentOrganization(org);
                    org = tempOrg;
                    break;
            }
        }

        private Person checkPersonExist(Person person) {
            if (persons.containsKey(person)) {
                return persons.get(person);
            } else {
                persons.put(person, person);
                return person;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            content = String.copyValueOf(ch, start, length).trim();
        }

        public Set<Organization> getOrgs() {
            return orgs;
        }
    }
}
