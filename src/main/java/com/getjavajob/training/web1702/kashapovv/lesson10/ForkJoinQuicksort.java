package com.getjavajob.training.web1702.kashapovv.lesson10;

import com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort.Quicksort;

import java.util.concurrent.ForkJoinPool;

/**
 * Created by Вадим on 02.04.2017.
 */
public class ForkJoinQuicksort implements Quicksort {
    @Override
    public <T extends Comparable<T>> T[] quickSort(T[] numbers) {
        ForkJoinPool pool = new ForkJoinPool();
        return pool.invoke(new ForkJoinQuicksortTask<>(numbers,0,numbers.length - 1));
    }
}
