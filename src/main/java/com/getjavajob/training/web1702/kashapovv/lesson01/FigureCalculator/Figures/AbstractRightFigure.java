package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.AbstractFigureBase;
import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures.AbstractFigure;

/**
 * Created by Вадим on 27.03.2017.
 */
public abstract class AbstractRightFigure extends AbstractFigure {
    protected AbstractRightFigure(AbstractFigureBase figureBase, double height) {
        super(figureBase, height);
    }

    @Override
    public double getHorizontalCrossSectionArea(double crossSectionHeight) {
        if (crossSectionHeight < 0) {
            throw new IllegalArgumentException("Cross Section Height must be positive number");
        }
        return getBaseArea();
    }

    @Override
    public double getSurfaceArea() {
        return 2 * getBaseArea() + getPerimeter() * getHeight();
    }

    @Override
    public double getFigureVolume() {
        return getBaseArea() * getHeight();
    }
}