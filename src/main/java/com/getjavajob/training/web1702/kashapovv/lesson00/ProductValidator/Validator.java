package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers.AbstractProductAttributeChecker;

/**
 * Created by Вадим on 23.02.2017.
 */
public class Validator {
    private AbstractProductAttributeChecker[] checkers;

    public Validator(AbstractProductAttributeChecker[] checkers) {
        if (checkers == null) {
            throw new NullPointerException("Checkers not exist");
        }
        this.checkers = checkers;
    }

    public void setCheckers(AbstractProductAttributeChecker[] checkers) {
        this.checkers = checkers;
    }

    public boolean validate(Product product) {
        for (AbstractProductAttributeChecker checker : checkers) {
            if (!checker.validate(product)) {
                return false;
            }
        }
        return true;
    }
}
