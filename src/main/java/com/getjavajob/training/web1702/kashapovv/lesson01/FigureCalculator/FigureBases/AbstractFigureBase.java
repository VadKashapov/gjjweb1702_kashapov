package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

/**
 * Created by Вадим on 22.02.2017.
 */
public abstract class AbstractFigureBase implements Cloneable {
    public abstract double getBaseArea();

    public abstract double getPerimeter();

    @Override
    public AbstractFigureBase clone() {
        try {
            return (AbstractFigureBase) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
