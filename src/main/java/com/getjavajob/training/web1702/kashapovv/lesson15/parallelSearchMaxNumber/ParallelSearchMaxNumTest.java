package com.getjavajob.training.web1702.kashapovv.lesson15.parallelSearchMaxNumber;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 10.05.2017.
 */
public class ParallelSearchMaxNumTest {
    @Test
    public void searchMaxElementOfArrayWithRemainder() throws Exception {
        int[] nums = new int[155_555];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = i;
        }
            assertEquals(155_554, new ParallelSearchMaxNum().search(nums));
        }

    @Test
    public void searchMaxElementOfArray() throws Exception {
        int[] nums = new int[1_500_000];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = i;
        }
        assertEquals(1_499_999, new ParallelSearchMaxNum().search(nums));
    }

    @Test
    public void searchMaxElementOfArrayOneElement() throws Exception {
        int[] nums = new int[1_000_000];
        Arrays.fill(nums,0);
        nums[49_000] = 10;
        assertEquals(10, new ParallelSearchMaxNum().search(nums));
    }
}