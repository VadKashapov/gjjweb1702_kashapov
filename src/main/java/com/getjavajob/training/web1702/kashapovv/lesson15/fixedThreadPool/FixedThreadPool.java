package com.getjavajob.training.web1702.kashapovv.lesson15.fixedThreadPool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Вадим on 06.05.2017.
 */
public class FixedThreadPool {
    Collection<Thread> threads;
    BlockingQueue<Runnable> tasks;
    private int poolSize;
    private boolean stop = false;

    public void stop() {
        this.stop = false;
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    public FixedThreadPool(int poolSize) {
        this.poolSize = poolSize;
        threads = new ArrayList<>();
        tasks = new LinkedBlockingQueue<>();
        for (int i = 0; i < poolSize; i++) {
            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    runWorker();
                }
            }));
        }
        for (Thread thread : threads) {
            thread.start();
        }
    }

    public void execute(Runnable task) {
        try {
            tasks.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runWorker() {
        while (!stop) {
            try {
                Runnable task = tasks.take();
                task.run();
            } catch (InterruptedException e) {
              //  e.printStackTrace();
            }
        }
    }
}
