package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class EmailChecker extends AbstractProductAttributeChecker {

    @Override
    public boolean validate(Product product) {
        String clientEmail = product.getClientEmail();
        return clientEmail.matches("[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-\\.]+");
    }
}
