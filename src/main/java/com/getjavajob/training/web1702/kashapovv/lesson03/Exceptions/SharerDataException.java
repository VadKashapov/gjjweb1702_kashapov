package com.getjavajob.training.web1702.kashapovv.lesson03.Exceptions;

import java.io.IOException;

/**
 * Created by Вадим on 05.03.2017.
 */
public class SharerDataException extends IOException {
    public SharerDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
