package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

import static java.lang.Math.PI;
import static java.lang.Math.tan;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Polygon extends AbstractFigureBase {
    private int cornerCount;
    private double edgeLength;

    public Polygon(int cornerCount, double edgeLength) {
        if (cornerCount <= 2) {
            throw new IllegalArgumentException("Number of corners must be >= 3");
        } else if (edgeLength < 0) {
            throw new IllegalArgumentException("Edge length must be positive number");
        }
        this.cornerCount = cornerCount;
        this.edgeLength = edgeLength;
    }

    @Override
    public double getBaseArea() {
        return edgeLength * cornerCount * getInCircleRadius() / 2;
    }

    public double getInCircleRadius() {
        return edgeLength / (2 * tan(PI / cornerCount));
    }

    public double getEdgeLength() {
        return edgeLength;
    }

    @Override
    public double getPerimeter() {
        return edgeLength * cornerCount;
    }

    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    public int getCornerCount() {
        return cornerCount;
    }

    public void setCornerCount(int cornerCount) {
        this.cornerCount = cornerCount;
    }

    @Override
    public int hashCode() {
        return 31 + cornerCount + Double.valueOf(edgeLength).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Polygon cObj = ((Polygon) obj);
        return cornerCount == cObj.getCornerCount() && edgeLength == cObj.getEdgeLength();
    }

    @Override
    public String toString() {
        return "Polygon has: " + cornerCount + " corners, " + edgeLength + " edge length";
    }
}
