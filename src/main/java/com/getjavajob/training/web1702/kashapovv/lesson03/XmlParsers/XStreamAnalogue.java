package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.XmlAlias;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.XmlAttribute;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.XmlTransient;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by Вадим on 10.03.2017.
 */
public class XStreamAnalogue {
    private Deque<Integer> whitespaceCount;
    private XMLStreamReader reader;

    public XStreamAnalogue() {
    }

    public String toXml(Object source) throws ParserConfigurationException, IllegalAccessException, IOException {
        whitespaceCount = new ArrayDeque<>();
        whitespaceCount.add(0);
        Element root = getComplexElement(source.getClass().getName(), source);
        return root.toString();
    }

    public Object fromXml(InputStream istream) throws Exception {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        reader = factory.createXMLStreamReader(istream);
        return readElements();
    }

    private Object readElements() throws XMLStreamException, InstantiationException, IllegalAccessException
            , java.lang.reflect.InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        Deque<Object> objectsStack = new ArrayDeque<>();
        Deque<Class> typeStack = new ArrayDeque<>();
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    Class recognizedClass = getClass(reader.getLocalName());
                    try {
                        if (recognizedClass == null) {
                            Field field = objectsStack.peek().getClass().getDeclaredField(reader.getLocalName());
                            Class type = field.getType();
                            typeStack.push(type);
                            field.setAccessible(true);
                            if (isCollection(type)) {
                                Collection col = getCollection(type);
                                field.set(objectsStack.peek(), col);
                                objectsStack.push(col);
                                continue;
                            }
                            if (isSimpleType(type)) {
                                reader.next();
                                field.set(objectsStack.peek(), getSimpleType(type, reader.getText().trim()));
                            } else {
                                objectsStack.push(type.getConstructor().newInstance());
                                getAndSetAttributes(objectsStack.peek());
                            }
                        } else if (isCollection(recognizedClass)) {
                            typeStack.push(recognizedClass);
                            objectsStack.push(getCollection(recognizedClass));
                        } else {
                            typeStack.push(recognizedClass);
                            objectsStack.push(recognizedClass.getConstructor().newInstance());
                            getAndSetAttributes(objectsStack.peek());
                        }
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    Class type = typeStack.pop();
                    if (typeStack.isEmpty()) {
                        continue;
                    }
                    if (!isSimpleType(type)) {
                        Object obj = objectsStack.pop();
                        if (isCollection(objectsStack.peek().getClass())) {
                            ((Collection) objectsStack.peek()).add(obj);
                        } else {
                            try {
                                Field field = objectsStack.peek().getClass().getDeclaredField(reader.getLocalName());
                                field.setAccessible(true);
                                field.set(objectsStack.peek(), obj);
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
            }
        }
        return objectsStack.pop();
    }

    private void getAndSetAttributes(Object obj) throws NoSuchFieldException, IllegalAccessException {
        int attrCount = reader.getAttributeCount();
        if (attrCount != 0) {
            for (int i = 0; i < attrCount; i++) {
                Field attrField = obj.getClass().getDeclaredField(reader.getAttributeLocalName(i));
                attrField.setAccessible(true);
                attrField.set(obj, getSimpleType(attrField.getType(), reader.getAttributeValue(i)));
            }
        }
    }

    private boolean isSimpleType(Class type) {
        return type.isPrimitive() || type.isAssignableFrom(String.class) || type.isEnum();
    }

    private Object getSimpleType(Class type, String textValue) {
        if (type.isPrimitive()) {
            return getPrimitiveValue(textValue, type);
        } else if (type.isEnum()) {
            return Enum.valueOf(type, textValue);
        } else if (type.isAssignableFrom(String.class)) {
            return textValue;
        }
        return null;
    }

    private Collection getCollection(Class collectionClass) throws InstantiationException
            , IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        if (collectionClass.isInterface()) {
            switch (collectionClass.getSimpleName()) {
                case "List":
                    return new ArrayList();
                case "Set":
                    return new HashSet();
                case "Queue":
                case "Deque":
                    return new LinkedList();
            }
        }
        return (Collection) collectionClass.getDeclaredConstructor().newInstance();
    }

    private boolean isCollection(Class aClass) {
        return aClass == null ? false : Collection.class.isAssignableFrom(aClass);
    }

    private Number getPrimitiveValue(String value, Class type) {
        if (type.equals(int.class)) {
            return Integer.valueOf(value);
        } else if (type.equals(double.class)) {
            return Double.valueOf(value);
        } else if (type.equals(float.class)) {
            return Float.valueOf(value);
        } else if (type.equals(long.class)) {
            return Long.valueOf(value);
        }
        return null;
    }

    private Class getClass(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    private Element getComplexElement(String elementName, Object element) throws IllegalAccessException {
        Object[] values;
        List<Element> elementList = new ArrayList<>();
        if (element instanceof Collection) {
            values = ((Collection) element).toArray();
        } else if (element instanceof Map) {
            values = ((Map) element).values().toArray();
        } else if (element instanceof Object[]) {
            values = ((Object[]) element);
        } else {
            return getElement(elementName, element);
        }
        for (int i = 0; i < values.length; i++) {
            elementList.add(getElement(values[i].getClass().getName(), values[i]));
        }
        return new Element(elementName, elementList);
    }

    private Element getElement(String elementName, Object element) throws IllegalAccessException {
        XmlAlias elementAlias = element.getClass().getAnnotation(XmlAlias.class);
        if (elementAlias != null && elementName.equals(element.getClass().getName())) {
            elementName = elementAlias.value();
        }
        Field[] fields = element.getClass().getDeclaredFields();
        List<Element> elementList = new ArrayList<>();
        for (int j = 0; j < fields.length; j++) {
            if (fields[j].getAnnotation(XmlTransient.class) != null) {
                continue;
            }
            fields[j].setAccessible(true);
            String fieldName = fields[j].getName();
            Object elementValue = fields[j].get(element);
            if (elementValue == null) {
                continue;
            }
            Boolean attribute = fields[j].getAnnotation(XmlAttribute.class) != null;
            XmlAlias fieldAlias = fields[j].getAnnotation(XmlAlias.class);
            if (fieldAlias != null) {
                fieldName = fieldAlias.value();
            }
            Class type = fields[j].getType();

            if (type.isAssignableFrom(String.class)) {
                elementList.add(new Element(fieldName, (String) fields[j].get(element), attribute));
            } else if (type.isPrimitive() || type.isEnum()) {
                elementList.add(new Element(fieldName, elementValue.toString(), attribute));
            } else {
                elementList.add(getComplexElement(fieldName, elementValue));
            }
        }
        return new Element(elementName, elementList);
    }

    private class Element {
        private List<Element> childElementList;
        private String elementName;
        private String textValue;
        private boolean attribute;


        public Element(String elementName, String textValue, Boolean attribute) {
            this.elementName = elementName;
            this.textValue = textValue;
            this.attribute = attribute;
        }

        public Element(String elementName, List<Element> childElementList) {
            this.childElementList = childElementList;
            this.elementName = elementName;
        }

        private String printChildElementList() {
            StringBuilder builder = new StringBuilder();
            for (Element childElement : childElementList) {
                if (!childElement.attribute) {
                    builder.append(childElement.toString());
                }
            }
            whitespaceCount.pop(); //get parent tag indention on top stack
            return builder.toString();
        }

        private String printAttributeValueList() {
            if (childElementList == null || childElementList.isEmpty()) {
                return "";
            }
            StringBuilder builder = new StringBuilder();
            for (Element childElement : childElementList) {
                if (childElement.attribute) {
                    builder.append(' ');
                    builder.append(childElement.elementName).append('=');
                    builder.append('\"').append(childElement.textValue).append('\"');
                }
            }
            return builder.toString();
        }

        @Override
        public String toString() {
            String openTag = '<' + elementName + printAttributeValueList() + '>';
            String closeTag = "</" + elementName + '>';
            if (textValue != null) {
                return getSpaceStr() + openTag + textValue + closeTag + '\n';
            }
            if (childElementList != null && !childElementList.isEmpty()) {
                String spaceStr = getSpaceStr();
                whitespaceCount.add(whitespaceCount.peek() + 2); // add child tag indention
                return spaceStr + openTag + '\n' + printChildElementList() + spaceStr + closeTag + '\n';
            }
            return null;
        }

        private String getSpaceStr() {
            if (whitespaceCount.peek() == 0) {
                return "";
            } else if (whitespaceCount.peek() < 0) {
                throw new IllegalArgumentException("incorrect whitespace count");
            }
            char[] chars = new char[whitespaceCount.peek()];
            Arrays.fill(chars, ' ');
            return String.valueOf(chars);
        }
    }
}
