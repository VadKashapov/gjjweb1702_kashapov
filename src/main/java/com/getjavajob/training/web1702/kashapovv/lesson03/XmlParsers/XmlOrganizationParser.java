package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import java.io.InputStream;

/**
 * Created by Вадим on 11.03.2017.
 */
public interface XmlOrganizationParser {
    Object readXml(InputStream inputStream) throws Exception;
}
