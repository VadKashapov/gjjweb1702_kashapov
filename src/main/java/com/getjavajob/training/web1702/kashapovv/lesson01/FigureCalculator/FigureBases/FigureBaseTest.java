package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 01.03.2017.
 */
public class FigureBaseTest {
    @Test
    public void hashCodeTest() throws Exception {
        assertEquals(new Circle(15).hashCode(), new Circle(15).hashCode());
        assertEquals(new Polygon(5, 4).hashCode(), new Polygon(5, 4).hashCode());
        assertEquals(new Square(5).hashCode(), new Square(5).hashCode());
        assertEquals(new Triangle(5).hashCode(), new Triangle(5).hashCode());

        assertEquals(new Polygon(3, 5).hashCode(), new Triangle(5).hashCode());
        assertEquals(new Polygon(4, 5).hashCode(), new Square(5).hashCode());
    }

    @Test
    public void equalsTest() throws Exception {
        assertEquals(true, new Circle(15).equals(new Circle(15)));
        assertEquals(true, new Polygon(3, 5).equals(new Polygon(3, 5)));
        assertEquals(true, new Square(5).equals(new Square(5)));
        assertEquals(true, new Triangle(5).equals(new Triangle(5)));

        assertEquals(false, new Polygon(3, 5).equals(new Triangle(5)));
        assertEquals(false, new Triangle(5).equals(new Polygon(3, 5)));
    }

    @Test
    public void toStringTest() throws Exception {
        assertEquals("Circle radius: 15.0", new Circle(15).toString());
        assertEquals("Polygon has: 4 corners, 5.0 edge length"
                , new Polygon(4, 5).toString());
        assertEquals("Square has 5.0 edge length"
                , new Square(5).toString());
        assertEquals("Triangle has 5.0 edge length"
                , new Triangle(5).toString());
    }

    @Test
    public void cloneTest() throws Exception {
        Circle circle = new Circle(15);
        Circle circleClone = (Circle) circle.clone();
        circleClone.setRadius(10);
        assertEquals(15, circle.getRadius(), 10e-5);

        Polygon polygon = new Polygon(4, 5);
        Polygon polyClone = (Polygon) polygon.clone();
        polyClone.setCornerCount(5);
        polyClone.setEdgeLength(10);
        assertEquals(4, polygon.getCornerCount());
        assertEquals(5, polygon.getEdgeLength(), 10e-5);
    }

}