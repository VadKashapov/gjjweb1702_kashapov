package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Circle extends AbstractFigureBase {
    private double radius;

    public Circle(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be positive number");
        }
        this.radius = radius;
    }

    @Override
    public double getBaseArea() {
        return PI * pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * PI * radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
