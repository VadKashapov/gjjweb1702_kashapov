package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public abstract class AbstractProductAttributeChecker {
    protected static StringBuilder exceptionMessageBuilder = new StringBuilder(" Reason: ");

    abstract public boolean validate(Product product) throws ValidationException;

    public boolean isNotEmpty(String str) {
        if (str == null) {
            exceptionMessageBuilder.append("value is null");
        } else if (str.isEmpty()) {
            exceptionMessageBuilder.append("value is Empty");
        } else {
            return true;
        }
        return false;
    }

    public boolean isNatural(int num) {
       if (num <= 0) {
           exceptionMessageBuilder.append("value <= 0, value = ").append(num);
       } else {
           return true;
       }
        return false;
    }

    public boolean isNatural(double num) {
        if (num <= 0) {
            exceptionMessageBuilder.append("value <= 0, value = ").append(num);
        } else {
            return true;
        }
        return false;
    }

    public boolean isPositive(double num) {
        if (num < 0) {
            exceptionMessageBuilder.append("value < 0, value = ").append(num);
        } else {
            return true;
        }
        return false;
    }

    public boolean isPositive(int num) {
        if (num < 0) {
            exceptionMessageBuilder.append("value < 0, value = ").append(num);
        } else {
            return true;
        }
        return false;
    }
}
