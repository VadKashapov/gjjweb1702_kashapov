package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases.AbstractFigureBase;

/**
 * Created by Вадим on 22.02.2017.
 */
public abstract class AbstractFigure {
    protected AbstractFigureBase figureBase;
    protected double height;

    protected AbstractFigure(AbstractFigureBase figureBase, double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height must be > 0");
        }
        this.figureBase = figureBase;
        this.height = height;
    }

    public double getBaseArea() {
        return figureBase.getBaseArea();
    }

    public double getPerimeter() {
        return figureBase.getPerimeter();
    }

    public abstract double getHorizontalCrossSectionArea(double crossSectionHeight);

    public abstract double getSurfaceArea();

    public abstract double getFigureVolume();

    public double getHeight() {
        return height;
    }

    protected void setHeight(double height) {
        this.height = height;
    }
}
