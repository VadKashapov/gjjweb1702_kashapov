package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentAlphabetCounter;

/**
 * Created by Вадим on 18.03.2017.
 */
public class LetterReader extends Thread implements Runnable {
    private char countedChar;
    private ConcurrentAlphabetCounter counter;

    public LetterReader(char ch, ConcurrentAlphabetCounter counter) {
        this.countedChar = ch;
        this.counter = counter;
    }

    @Override
    public void run() {
        int count = 0;
        String text = counter.getText().toUpperCase();
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            if (countedChar == ch) {
                count++;
            }
        }
        synchronized (counter.getLetterFrequency()) {
            counter.getLetterFrequency().put(countedChar, count);
        }
    }
}
