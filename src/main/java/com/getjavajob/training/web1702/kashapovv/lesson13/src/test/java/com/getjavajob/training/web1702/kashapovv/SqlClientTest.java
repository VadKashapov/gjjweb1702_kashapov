package com.getjavajob.training.web1702.kashapovv.lesson13.src.test.java.com.getjavajob.training.web1702.kashapovv;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Created by Вадим on 21.04.2017.
 */
public class SqlClientTest {
    private static SqlRunner client;

    @BeforeClass
    public static void createConnectionBeforeExecuteQuery() throws Exception {
        Properties prop = new Properties();
        prop.load(ClassLoader.getSystemClassLoader().getResourceAsStream("lesson13.properties"));
        String url = prop.getProperty("database.url");
        String username = prop.getProperty("database.username");
        String password = prop.getProperty("database.password");

        client = new SqlRunner();
        client.createDatabaseConnection(url, username, password);
    }

    @Test
    public void executeQueryTestCreateTable() throws Exception {
        client.executeQuery("CREATE TABLE `dbase`.`testtable` (\n" +
                "  `idtestTable` INT NOT NULL,\n" +
                "  `testTablecol` VARCHAR(45) NULL,\n" +
                "  PRIMARY KEY (`idtestTable`));\n");
        client.executeQuery("INSERT INTO `dbase`.`testtable` (`idtestTable`, `testTablecol`) VALUES ('1', 'testCol1'),\n" +
                "('2', 'testCol2'),\n('3', NULL);");
        Collection<String[]> rows = client.executeQuery("SELECT * FROM dbase.testtable;");
        Collection<String[]> expectRows = new ArrayList<>();
        expectRows.add(new String[]{"idtestTable", "testTablecol"});
        expectRows.add(new String[]{"1", "testCol1"});
        expectRows.add(new String[]{"2", "testCol2"});
        expectRows.add(new String[]{"3", ""});
        assertArrayEquals(rows.toArray(), expectRows.toArray());
        client.executeQuery("DROP TABLE testtable;");
    }

    @Test
    public void createDatabaseConnectionTest() throws Exception {
        Properties prop = new Properties();
        prop.load(getClass().getClassLoader().getResourceAsStream("lesson13.properties"));
        String url = prop.getProperty("database.url");
        String username = prop.getProperty("database.username");
        String password = prop.getProperty("database.password");

        client = new SqlRunner();
        assertTrue(client.createDatabaseConnection(url, username, password));
        client.closeConnection();
    }
}
