package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.Circle;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Cylinder extends AbstractRightFigure {

    public Cylinder(double baseRadius, double height) {
        super(new Circle(baseRadius), height);
    }
}
