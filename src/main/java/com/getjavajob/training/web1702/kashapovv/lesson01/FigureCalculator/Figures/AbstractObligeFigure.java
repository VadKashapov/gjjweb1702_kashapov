package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.AbstractFigureBase;

/**
 * Created by Вадим on 26.03.2017.
 */
public abstract class AbstractObligeFigure extends AbstractFigure {

    protected AbstractObligeFigure(AbstractFigureBase figureBase, double height) {
        super(figureBase, height);
    }

    @Override
    public double getFigureVolume() {
        return getBaseArea() * getHeight() / 3;
    }
}
