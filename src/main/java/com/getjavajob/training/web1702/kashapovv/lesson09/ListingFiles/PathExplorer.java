package com.getjavajob.training.web1702.kashapovv.lesson09.ListingFiles;

import java.io.IOException;
import java.nio.file.*;

/**
 * Created by Вадим on 19.03.2017.
 */
public class PathExplorer {
    public static void main(String[] args) {
        printPathContent(ClassLoader.getSystemResource("").getPath().substring(1), "xml");
    }

    public static void printPathContent(String dirPath, String fileExt) {
        try {
            Files.walkFileTree(Paths.get(dirPath), new FileVisitor<Path>(fileExt));
        } catch (NoSuchFileException | InvalidPathException e) {
            throw new IllegalArgumentException("Invalid path: " + dirPath,e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
