package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases.Polygon;

import static com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases.PolygonBuilder.getPolygonFigure;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Pyramid extends AbstractObligeFigure {

    public Pyramid(int cornerCount, double edgeLength, double height) {
        super(getPolygonFigure(cornerCount, edgeLength), height);
    }

    @Override
    public double getHorizontalCrossSectionArea(double crossSectionHeight) {
        if (crossSectionHeight < 0) {
            throw new IllegalArgumentException("Cross Section Height must be positive number");
        }
        Polygon poly = (Polygon) figureBase;
        double edgeLength = poly.getEdgeLength();
        double height = getHeight();
        return new Polygon(poly.getCornerCount(), edgeLength * (height - crossSectionHeight) / height).getBaseArea();
    }

    @Override
    public double getSurfaceArea() {
        Polygon poly = (Polygon) figureBase;
        return getBaseArea() + getPerimeter() * sqrt(pow(poly.getInCircleRadius(), 2)
                + pow(getHeight(), 2)) / 2;
    }

    @Override
    public double getFigureVolume() {
        return getBaseArea() * getHeight() / 3;
    }
}
