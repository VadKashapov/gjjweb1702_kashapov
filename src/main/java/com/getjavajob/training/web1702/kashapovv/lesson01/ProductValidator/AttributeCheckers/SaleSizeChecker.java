package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class SaleSizeChecker extends AbstractProductAttributeChecker {
    private static final double SALE_SIZE_ON_PRICE_RATIO = 0.2;

    @Override
    public boolean validate(Product product) throws ValidationException {
        double saleSize = product.getSaleSize();
        if (isPositive(saleSize)) {
            if (saleSize / (product.getProductPrice() * product.getQuantityRequired()) <= SALE_SIZE_ON_PRICE_RATIO) {
                return true;
            } else {
                double saleRatio = saleSize / (product.getProductPrice() * product.getQuantityRequired());
                exceptionMessageBuilder.insert(0,"Field: saleSize");
                exceptionMessageBuilder.append("saleSize / (productPrice * quantityRequired) >")
                        .append(SALE_SIZE_ON_PRICE_RATIO).append('.')
                        .append(" sale size ratio =").append(saleRatio)
                        .append(" saleSize = ").append(saleSize)
                        .append(" productPrice =").append(product.getProductPrice())
                        .append(" quantity required = ").append(product.getQuantityRequired())
                        .append(". Sale size ratio must be <= ").append(SALE_SIZE_ON_PRICE_RATIO);
            }
        } else {
            exceptionMessageBuilder.insert(0,"Field: saleSize");
        }
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}