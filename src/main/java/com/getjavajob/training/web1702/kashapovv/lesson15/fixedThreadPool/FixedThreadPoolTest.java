package com.getjavajob.training.web1702.kashapovv.lesson15.fixedThreadPool;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Вадим on 06.05.2017.
 */
public class FixedThreadPoolTest {
    @Test
    public void executeMultithreadedTask() throws Exception {
        FixedThreadPool fixedThreadPool = new FixedThreadPool(5);
        Runnable runnable = null;
        final int[] ints = new int[5];
        for (int i = 0; i < 5; i++) {
            final int copyI = i;
            runnable = new Runnable() {
                @Override
                public void run() {
                    System.out.println(copyI);
                    ints[copyI] = copyI + 1;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            fixedThreadPool.execute(runnable);
        }

        Thread.sleep(1000);
        fixedThreadPool.stop();

        assertArrayEquals(ints,new int[]{1,2,3,4,5});
    }
}