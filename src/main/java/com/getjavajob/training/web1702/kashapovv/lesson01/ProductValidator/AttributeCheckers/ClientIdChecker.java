package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class ClientIdChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) throws ValidationException {
        String clientId = product.getClientId();
        if (isNotEmpty(clientId)) {
            if (clientId.equals(clientId.toUpperCase())) {
                return true;
            } else {
                exceptionMessageBuilder.append("value not in uppercase");
                exceptionMessageBuilder.append(", ").append("value = ").append(clientId);
            }
        }
        exceptionMessageBuilder.insert(0, "Field: clientID");
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}
