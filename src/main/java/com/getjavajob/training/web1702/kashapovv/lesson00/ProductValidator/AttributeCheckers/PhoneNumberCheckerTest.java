package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 25.02.2017.
 */
public class PhoneNumberCheckerTest {
    private PhoneNumberChecker checker;
    private Product product;

    @Before
    public void setUp() throws Exception {
        checker = new PhoneNumberChecker();
        product = new Product();
    }

    @Test
    public void validateTest() throws Exception {
        product.setPhoneNumber("+79123456789");
        assertTrue(checker.validate(product));
    }

    @Test
    public void validatePhoneNumberWithoutPlusSign() throws Exception {
        product.setPhoneNumber("79123456789");
        assertFalse(checker.validate(product));
    }

    @Test
    public void validatePhoneNumberLessLength() throws Exception {
        product.setPhoneNumber("+7912345678");
        assertFalse(checker.validate(product));
    }

    @Test
    public void validatePhoneNumberWithWrongStartCode() throws Exception {
        product.setPhoneNumber("+78123456789");
        assertFalse(checker.validate(product));
    }

    @Test
    public void validatePhoneNumberWithParentheses() throws Exception {
        product.setPhoneNumber("+7(912)3456789");
        assertFalse(checker.validate(product));
    }

    @Test
    public void validatePhoneNumberWithNotNumChar() throws Exception {
        product.setPhoneNumber("+7912d456789");
        assertFalse(checker.validate(product));
    }
}