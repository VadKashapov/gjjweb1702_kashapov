package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator;

import com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.Figures.*;

import static com.getjavajob.training.web1702.kashapovv.Utils.ConsoleIO.readDouble;
import static com.getjavajob.training.web1702.kashapovv.Utils.ConsoleIO.readInt;

/**
 * Created by Вадим on 22.02.2017.
 */
public class GeometricFigureCalculator {
    public static void main(String[] args) {
        for (; ; ) {
            System.out.println("Choose figure type:\n 1 - Cone\n 2 - Pyramid\n 3 - Cylinder\n 4 - Prism\n " +
                    "0 - Exit program");
            switch (readInt()) {
                case 0:
                    return;
                case 1: {
                    System.out.println("Input cone base radius and height of cone:");
                    double baseRadius = readDouble();
                    double height = readDouble();
                    calculateFigure(new Cone(baseRadius, height));
                    break;
                }
                case 2: {
                    System.out.println("Input pyramid number of corners, pyramid base edge length, height of pyramid:");
                    int cornerCount = readInt();
                    double edgeLength = readDouble();
                    double height = readDouble();
                    calculateFigure(new Pyramid(cornerCount, edgeLength, height));
                    break;
                }
                case 3: {
                    System.out.println("Input cylinder base radius and height of cylinder:");
                    double baseRadius = readDouble();
                    double height = readDouble();
                    calculateFigure(new Cylinder(baseRadius, height));
                    break;
                }
                case 4: {
                    System.out.println("Input prism number of corners, prism base edge length and height of prism:");
                    int cornerCount = readInt();
                    double edgeLength = readDouble();
                    double height = readDouble();
                    calculateFigure(new Prism(cornerCount, edgeLength, height));
                    break;
                }
                default: {
                    System.out.println("Wrong input");
                    break;
                }
            }
        }
    }

    private static void calculateFigure(AbstractFigure figure) {
        System.out.printf("Figure: %s%nBase Area: %f%nSurface Area: %f%nFigure Volume: %f%n",
                figure.getClass().getSimpleName(), figure.getBaseArea(), figure.getSurfaceArea()
                , figure.getFigureVolume());
        System.out.println("Optional: Horizontal Cross Section Area. Input height of cross section (or 0 to skip): ");
        double crossSectionHeight = readDouble();
        if (crossSectionHeight == 0) {
            return;
        }
        System.out.println(figure.getHorizontalCrossSectionArea(crossSectionHeight));
    }
}
