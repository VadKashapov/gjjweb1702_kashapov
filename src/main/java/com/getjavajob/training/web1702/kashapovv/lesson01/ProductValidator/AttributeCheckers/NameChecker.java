package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class NameChecker extends AbstractProductAttributeChecker {
    public boolean validate(Product product) throws ValidationException {
        String productName = product.getProductName();
        if (isNotEmpty(productName)) {
            return true;
        }
        exceptionMessageBuilder.insert(0,"Field: productName");
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}
