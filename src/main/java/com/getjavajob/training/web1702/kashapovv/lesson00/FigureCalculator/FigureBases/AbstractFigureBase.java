package com.getjavajob.training.web1702.kashapovv.lesson00.FigureCalculator.FigureBases;

/**
 * Created by Вадим on 22.02.2017.
 */
public abstract class AbstractFigureBase {
    public abstract double getBaseArea();

    public abstract double getPerimeter();
}
