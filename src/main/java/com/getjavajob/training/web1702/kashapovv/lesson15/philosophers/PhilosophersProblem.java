package com.getjavajob.training.web1702.kashapovv.lesson15.philosophers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Created by Вадим on 06.05.2017.
 */
public class PhilosophersProblem {
    public static final int FORK_NUM = 5;
    public static final int PHILOSOPHERS = 5;
    public static final int FORK_LIMIT_WITHOUT_WAIT = 4;
    private boolean stop = false;

    public static void main(String[] args) {
        new PhilosophersProblem().run();
    }

    private void run() {
        ExecutorService service = newFixedThreadPool(PHILOSOPHERS);
        Semaphore forkWatcher = new Semaphore(FORK_LIMIT_WITHOUT_WAIT);
        int[][] forkPairs = {{4, 0}, {0, 1}, {1, 2}, {2, 3}, {3, 4}};
        Lock[] forks = new Lock[FORK_NUM];
        for (int i = 0; i < 5; i++) {
            forks[i] = new ReentrantLock();
        }
        for (int i = 0; i < 5; i++) {
            Runnable task = getTask(forkWatcher, forks, forkPairs[i][0], forkPairs[i][1]);
            service.execute(task);
        }
        try {
            Thread.sleep(3000);
            stop = true;
            service.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Runnable getTask(final Semaphore forkWatcher, final Lock[] forks, final int leftForkNum
            , final int rightForkNum) {
        return new Runnable() {
            private Lock leftFork = forks[leftForkNum];
            private Lock rightFork = forks[rightForkNum];

            @Override
            public void run() {
                while (!stop) {
                    try {
                        System.out.println(Thread.currentThread() + " think");
                        Thread.sleep(100);
                        forkWatcher.acquire();
                        leftFork.lock();
                        rightFork.lock();
                        System.out.println(Thread.currentThread() + " eat with " + leftForkNum + ',' + rightForkNum);
                        Thread.sleep(100);
                        rightFork.unlock();
                        leftFork.unlock();
                        forkWatcher.release();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }
}
