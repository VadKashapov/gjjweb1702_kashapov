package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product {
    private final static String DATE_FORMAT = "dd.MM.yyyy";

    private static SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    @FieldDescription("Product Name")
    private String productName;

    @FieldDescription("Quantity product required")
    private int quantityRequired;

    @FieldDescription("Quantity product available")
    private int quantityAvailable;

    @FieldDescription("Product price")
    private double productPrice;

    @FieldDescription("Sale size")
    private double saleSize;

    @FieldDescription("Client ID")
    private String clientId;

    @FieldDescription("Client email")
    private String clientEmail;

    @FieldDescription("Phone number")
    private String phoneNumber;

    @FieldDescription("Shipping date")
    private Date shippingDate;

    public Product() {
    }

    public Product(String productName, int quantityRequired, int quantityAvailable, double productPrice,
                   double saleSize, String clientId, String clientEmail, String phoneNumber, String shippingDate) {
        this.productName = productName;
        this.quantityRequired = quantityRequired;
        this.quantityAvailable = quantityAvailable;
        this.productPrice = productPrice;
        this.saleSize = saleSize;
        this.clientId = clientId;
        this.clientEmail = clientEmail;
        this.phoneNumber = phoneNumber;

        try {
            this.shippingDate = dateFormat.parse(shippingDate);
        } catch (ParseException e) {
            System.out.println("invalid date format, should be: dd/MM/yyyy");
            e.printStackTrace();
        }
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantityRequired() {
        return quantityRequired;
    }

    public void setQuantityRequired(int quantityRequired) {
        this.quantityRequired = quantityRequired;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getSaleSize() {
        return saleSize;
    }

    public void setSaleSize(double saleSize) {
        this.saleSize = saleSize;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        try {
            this.shippingDate = dateFormat.parse(shippingDate);
        } catch (ParseException e) {
            System.out.println("invalid date format, should be: dd/MM/yyyy");
            e.printStackTrace();
        }
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", quantityRequired=" + quantityRequired +
                ", quantityAvailable=" + quantityAvailable +
                ", productPrice=" + productPrice +
                ", saleSize=" + saleSize +
                ", clientId='" + clientId + '\'' +
                ", clientEmail='" + clientEmail + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", shippingDate=" + shippingDate +
                '}';
    }
}
