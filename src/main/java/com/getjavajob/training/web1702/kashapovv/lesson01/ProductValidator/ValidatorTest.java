package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers.*;
import org.junit.Test;

/**
 * Created by Вадим on 24.02.2017.
 */
public class ValidatorTest {
    private Validator validator;

    @Test(expected = ValidationException.class)
    public void validate() throws Exception {
        validator = new Validator(new AbstractProductAttributeChecker[]{new NameChecker()
                , new QuantityRequiredChecker()
                , new PriceChecker(), new SaleSizeChecker(), new ClientIdChecker(), new EmailChecker()
                , new PhoneNumberChecker(), new ShippingDateChecker()});
        validator.validate(new Product("Smetana", 5, 30, 55.00, 0
                , "A001", null, "+78850000000", "27.02.2017"));
    }
}