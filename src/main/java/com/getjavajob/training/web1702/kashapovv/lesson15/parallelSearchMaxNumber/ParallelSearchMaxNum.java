package com.getjavajob.training.web1702.kashapovv.lesson15.parallelSearchMaxNumber;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Created by Вадим on 06.05.2017.
 */
public class ParallelSearchMaxNum {
    public static final int THRESHOLD = 10000;
    private AtomicInteger currentMax = new AtomicInteger(0);

    public int search(int[] nums) {
        ExecutorService service = newFixedThreadPool(4);
        int iter;
        int remain = 0;
        if (nums.length < THRESHOLD) {
            service.execute(getTask(0, nums.length, nums));
        } else {
            iter = nums.length / THRESHOLD;
            remain = nums.length % THRESHOLD;
            for (int chunk = 0; chunk < iter; chunk++) {
                service.execute(getTask(chunk * THRESHOLD, (chunk + 1) * THRESHOLD, nums));
            }
            if (remain != 0) {
                service.execute(getTask(iter * THRESHOLD, iter * THRESHOLD + remain, nums));
            }
        }

        try {
            service.shutdown();
            service.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return currentMax.get();
    }

    private Runnable getTask(final int start, final int end, final int[] nums) {
        return new Runnable() {
            @Override
            public void run() {
                for (int i = start; i < end; i++) {
                    if (nums[i] > currentMax.get()) {
                        currentMax.set(nums[i]);
                    }
                }
            }
        };
    }

    public static void main(String[] args) {
        int[] nums = new int[155_000];
        Random random = new Random(100);
        for (int i = 0; i < nums.length; i++) {
            nums[i] = i;
        }
        System.out.println(new ParallelSearchMaxNum().search(nums));
    }
}
