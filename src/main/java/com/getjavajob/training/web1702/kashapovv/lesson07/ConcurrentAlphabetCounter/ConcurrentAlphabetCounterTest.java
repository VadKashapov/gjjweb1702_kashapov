package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentAlphabetCounter;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map.Entry;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 18.03.2017.
 */
public class ConcurrentAlphabetCounterTest {
    @Test
    public void allAlphabetsOnce() throws Exception {
        String source = "АбвгдеЁжзийкЛмноПрстуФхцчшЩъыьэЮя";
        Charset cp1251 = Charset.forName("CP1251");
        List<Entry<Character, Integer>> letterFrequencyEntries =
                new ConcurrentAlphabetCounter().cyrillicLetterFrequency(new ByteArrayInputStream(
                        source.getBytes(cp1251)), cp1251);
        for (Entry<Character, Integer> letterFrequencyEntry : letterFrequencyEntries) {
            assertEquals(1, ((int) letterFrequencyEntry.getValue()));
        }
    }

    @Test
    public void oneAlphabet() throws Exception {
        String source = "ааааАААА";
        Charset cp1251 = Charset.forName("CP1251");
        List<Entry<Character, Integer>> letterFrequencyEntries =
                new ConcurrentAlphabetCounter().cyrillicLetterFrequency(new ByteArrayInputStream(
                        source.getBytes(cp1251)), cp1251);
        assertEquals(8, ((int) letterFrequencyEntries.get(0).getValue()));
    }
}