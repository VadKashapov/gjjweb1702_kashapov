package com.getjavajob.training.web1702.kashapovv.lesson13.src.main.java.com.getjavajob.training.web1702.kashapovv;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Вадим on 02.05.2017.
 */
public class SqlRunner {
    private Connection connection;

    public List<String[]> executeQuery(String query) throws SQLException {
        try (Statement stat = connection.createStatement()) {
            stat.execute(query);
            try (ResultSet resultSet = stat.getResultSet()) {
                connection.commit();
                return getValues(resultSet);
            } catch (SQLException e) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    //empty
                }
                throw e;
            }
        }
    }

    private List<String[]> getValues(ResultSet resultSet) throws SQLException {
        if (resultSet == null) {
            return null;
        }
        ResultSetMetaData resultSetMeta = resultSet.getMetaData();
        int columnNumber = resultSetMeta.getColumnCount();

        List<String[]> rows = new ArrayList<>();
        String[] columnValues = new String[columnNumber];

        for (int i = 1; i <= columnNumber; i++) {
            columnValues[i - 1] = resultSetMeta.getColumnLabel(i);
        }
        rows.add(columnValues);

        while (resultSet.next()) {
            columnValues = new String[columnNumber];
            for (int i = 1; i <= columnNumber; i++) {
                String str;
                columnValues[i - 1] = (str = resultSet.getString(i)) != null ? str : "";
            }
            rows.add(columnValues);
        }
        return rows;
    }

    public boolean createDatabaseConnection(String url, String username, String password) throws SQLException {
        connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return true;
    }

    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
}
