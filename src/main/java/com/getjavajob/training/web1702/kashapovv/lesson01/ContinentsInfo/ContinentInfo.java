package com.getjavajob.training.web1702.kashapovv.lesson01.ContinentsInfo;

import static com.getjavajob.training.web1702.kashapovv.Utils.ConsoleIO.readString;

/**
 * Created by Вадим on 01.03.2017.
 */
public class ContinentInfo {
    public static void main(String[] args) {
        try {
            System.out.println("Input next continent to see info:");
            StringBuilder builder = new StringBuilder();
            for (Continent cont : Continent.values()) {
                builder.append(cont.name()).append("\n");
            }
            System.out.println(builder);
            printContinentInfo(readString());
        } catch (IllegalArgumentException e) {
            System.out.println("Wrong input or continent info not found");
        }
    }

    public static void printContinentInfo(String name) {
        System.out.println(Continent.valueOf(name.toUpperCase()).toString());
    }

    public enum Continent {
        AMERICA(24.250, 565.265, 23), EUROPE(10.18, 740.000, 50), ASIA(44.579, 4_164.252, 49), AFRICA(30.221_532, 1_100.000, 55), AUSTRALIA(7.692_024, 24.067_700, 1), ANTARCTICA(14.107_000, 0, 0);

        private double area;
        private double population;
        private int countryNumber;

        Continent(double area, double population, int countryNumber) {
            this.area = area;
            this.population = population;
            this.countryNumber = countryNumber;
        }

        @Override
        public String toString() {
            return this.name() +
                    "\narea=" + area +
                    ", population= " + population +
                    ", countryNumber= " + countryNumber;
        }
    }
}
