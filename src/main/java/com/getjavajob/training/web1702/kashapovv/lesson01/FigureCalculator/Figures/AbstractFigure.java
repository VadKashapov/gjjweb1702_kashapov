package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.AbstractFigureBase;

/**
 * Created by Вадим on 22.02.2017.
 */
public abstract class AbstractFigure implements Cloneable {
    protected AbstractFigureBase figureBase;
    protected double height;

    protected AbstractFigure(AbstractFigureBase figureBase, double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height must be > 0");
        }
        this.figureBase = figureBase;
        this.height = height;
    }

    public double getBaseArea() {
        return figureBase.getBaseArea();
    }

    public double getPerimeter() {
        return figureBase.getPerimeter();
    }

    public abstract double getHorizontalCrossSectionArea(double crossSectionHeight);

    public abstract double getSurfaceArea();

    public abstract double getFigureVolume();

    public double getHeight() {
        return height;
    }

    protected void setHeight(double height) {
        this.height = height;
    }

    @Override
    public int hashCode() {
        return figureBase.hashCode() + Double.valueOf(height).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractFigure cObj = ((AbstractFigure) obj);
        return this.hashCode() == cObj.hashCode();
    }

    @Override
    protected AbstractFigure clone() {
        try {
            AbstractFigure clone = (AbstractFigure) super.clone();
            clone.figureBase = clone.figureBase.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
       return null;
    }

    @Override
    public String toString() {
        return "Figure: " + this.getClass().getSimpleName() + "\nHeight: " + getHeight() + " and base :\n" +
                figureBase.toString();
    }
}
