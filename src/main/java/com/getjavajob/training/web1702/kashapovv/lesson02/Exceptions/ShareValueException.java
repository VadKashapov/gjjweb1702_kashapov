package com.getjavajob.training.web1702.kashapovv.lesson02.Exceptions;

import java.io.IOException;

/**
 * Created by Вадим on 03.03.2017.
 */
public class ShareValueException extends IOException {
    public ShareValueException(String message) {
        super(message);
    }
}
