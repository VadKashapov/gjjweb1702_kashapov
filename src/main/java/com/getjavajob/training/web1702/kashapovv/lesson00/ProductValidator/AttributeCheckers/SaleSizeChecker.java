package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class SaleSizeChecker extends AbstractProductAttributeChecker {
    private static final double SALE_SIZE_ON_ORDER_PRICE = 0.2;

    @Override
    public boolean validate(Product product) {
        double saleSize = product.getSaleSize();
        return saleSize >= 0 && saleSize / (product.getProductPrice() * product.getQuantityRequired()) <= SALE_SIZE_ON_ORDER_PRICE;
    }
}
