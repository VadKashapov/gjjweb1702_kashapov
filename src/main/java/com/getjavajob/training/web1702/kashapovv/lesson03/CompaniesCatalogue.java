package com.getjavajob.training.web1702.kashapovv.lesson03;

import com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders.CatalogueReadFile;
import com.getjavajob.training.web1702.kashapovv.lesson03.FileWriteReaders.CatalogueReadWriteData;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationParser;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Вадим on 02.03.2017.
 */
public class CompaniesCatalogue {
    public final static String FILE_NAME = "Organizations.dat";

    private Set<Organization> orgs;

    public CompaniesCatalogue() {
        URL baseUrl = this.getClass().getResource(FILE_NAME);
        if (baseUrl != null) {
            Set<Organization> orgs = CatalogueReadWriteData.readData(new File(baseUrl.getPath()));
            if (orgs != null) {
                this.orgs = orgs;
            } else {
                this.orgs = new HashSet<>();
            }
        } else {
            this.orgs = new HashSet<>();
        }
    }

    public static void main(String[] args) {
        CompaniesCatalogue catalogue = new CompaniesCatalogue();
        if (args[0] != null) {
            String filePath = CompaniesCatalogue.class.getResource(args[0]).getPath();
            catalogue.readFile(new File(filePath));
        }
        //     catalogue.readConsole();
        System.out.println("Most richest person: " + catalogue.lastNameRichestPersonAtAll());
        System.out.println("Organization with max subsidiaries: " + catalogue.orgNameWithMaxSubsidiaries());
        catalogue.saveData();
    }

    public void readFile(File file) {
        Set<Organization> newOrgs = CatalogueReadFile.readFile(file);
        if (newOrgs != null) {
            orgs.addAll(newOrgs);
        }
    }

    private void saveData() {
        CatalogueReadWriteData.writeData(orgs, CompaniesCatalogue.class.getResource("").getPath());
    }

    public void readConsole() {
        System.out.println("Input organization info in program appropriate format (Empty line for ending):");
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        OrganizationParser parser = new OrganizationParser(orgs);
        try {
            for (String line = bufReader.readLine(); !line.equals(""); line = bufReader.readLine()) {
                orgs.add(parser.parseLine(line));
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public String orgNameWithMaxSubsidiaries() {
        Map<Organization, Integer> orgsSubsidiaryCount = new HashMap<>();

        for (Organization org : orgs) {
            orgsSubsidiaryCount.put(org, 0);
        }
        for (Organization org : orgs) {
            while (org.getParentOrganization() != null) {
                orgsSubsidiaryCount.put(org.getParentOrganization()
                        , orgsSubsidiaryCount.get(org.getParentOrganization()) + 1);
                org = org.getParentOrganization();
            }
        }

        Map.Entry<Organization, Integer> maxEntry = null;

        for (Map.Entry<Organization, Integer> entry : orgsSubsidiaryCount.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry != null && maxEntry.getValue() != 0 ? maxEntry.getKey().getName() : null;
    }

    public String lastNameRichestPersonAtAll() {
        Map<Shareholder, Double> sharersWealth = new HashMap<>();

        for (Organization org : orgs) {
            for (Shareholder sharer : org.getShareholders()) {
                Double sharerMoney = org.getStatedCapital() * sharer.getSharesProportion() / 100.00;
                Double personMoneyOverall = sharersWealth.put(sharer, sharerMoney);
                if (personMoneyOverall != null) {
                    sharersWealth.put(sharer, sharerMoney + personMoneyOverall);
                }
            }
        }

        Map.Entry<Shareholder, Double> maxEntry = null;

        for (Map.Entry<Shareholder, Double> entry : sharersWealth.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry != null ? maxEntry.getKey().getPerson().getLastName() : null;
    }
}