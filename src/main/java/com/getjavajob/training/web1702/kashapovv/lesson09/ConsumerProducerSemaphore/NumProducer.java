package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerSemaphore;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by Вадим on 22.03.2017.
 */
public class NumProducer extends Thread implements Runnable {
    private Semaphore mySemaphore;
    private Semaphore evenThreadSemaphore;
    private Semaphore oddThreadSemaphore;
    private Random random = new Random();

    public NumProducer(Semaphore evenThreadSemaphore, Semaphore oddThreadSemaphore, Semaphore mySemaphore) {
        this.evenThreadSemaphore = evenThreadSemaphore;
        this.oddThreadSemaphore = oddThreadSemaphore;
        this.mySemaphore = mySemaphore;
    }

    @Override
    public void run() {
        while (true) {
            try {
                mySemaphore.acquire();
                int value = random.nextInt(10);
                Main.Data.value = value;
                if (value % 2 == 0) {
                    evenThreadSemaphore.release();
                } else {
                    oddThreadSemaphore.release();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
