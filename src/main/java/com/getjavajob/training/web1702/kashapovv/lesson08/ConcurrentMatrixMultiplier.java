package com.getjavajob.training.web1702.kashapovv.lesson08;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Вадим on 18.03.2017.
 */
public class ConcurrentMatrixMultiplier extends MatrixMultiplier {
    public static void main(String[] args) {
        ConcurrentMatrixMultiplier multiplier = new ConcurrentMatrixMultiplier();
        InputStream imatrix1 = multiplier.getClass().getResourceAsStream("matrix1.txt");
        InputStream imatrix2 = multiplier.getClass().getResourceAsStream("matrix2.txt");
        multiplier.multiplyMatrix(imatrix1, imatrix2);
    }

    @Override
    protected int[][] multiply(List<int[]> matrix1, List<int[]> matrix2) {
        if (matrix1.size() == 0 || matrix2.size() == 0 || matrix1.size() != matrix2.get(0).length) {
            throw new IllegalArgumentException("matrix1 size should be NxM and matrix2 size MxK");
        }
        int[][] resultMatrix = new int[matrix1.get(0).length][matrix2.size()];

        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < matrix1.get(0).length; i++) {
            threads.add(getMultiplierThread(i, matrix1, matrix2, resultMatrix));
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return resultMatrix;
    }

    private Thread getMultiplierThread(final int index, final List<int[]> matrix1
            , final List<int[]> matrix2, final int[][] resultMatrix) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                for (int j = 0; j < matrix2.size(); j++) {
                    resultMatrix[index][j] = matrix1.get(index)[j] * matrix2.get(j)[index];
                }
            }
        });
    }
}
