package com.getjavajob.training.web1702.kashapovv.lesson09.ListingFiles;

import org.junit.Test;

/**
 * Created by Вадим on 20.03.2017.
 */
public class PathExplorerTest {

    @Test(expected = IllegalArgumentException.class)
    public void incorrectPath() throws Exception {
        PathExplorer.printPathContent(").getPath()", "xml");
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectFirstChar() throws Exception {
        PathExplorer.printPathContent("/D:/dev/projects/getjavajob", "xml");
    }

    @Test
    public void correctPath() throws Exception {
        PathExplorer.printPathContent(ClassLoader.getSystemResource("").getPath().substring(1), "xml");
    }
}