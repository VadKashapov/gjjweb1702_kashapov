package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;

/**
 * Created by Вадим on 23.02.2017.
 */
public class PhoneNumberChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) {
        return product.getPhoneNumber().matches("^\\+79[0-9]{9}");
    }
}
