package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerExchanger;

import java.util.concurrent.Exchanger;

/**
 * Created by Вадим on 22.03.2017.
 */
public class NumConsumer implements Runnable {
    private Exchanger<Integer> exchanger;

    public NumConsumer(Exchanger<Integer> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        while (true) {
            try {
               int value = exchanger.exchange(null);
                System.out.println(value);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
