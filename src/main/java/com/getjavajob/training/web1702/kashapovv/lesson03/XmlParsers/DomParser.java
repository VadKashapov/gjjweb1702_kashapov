package com.getjavajob.training.web1702.kashapovv.lesson03.XmlParsers;

import com.getjavajob.training.web1702.kashapovv.lesson03.Exceptions.ParentOrgException;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Organization;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.OrganizationType;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Person;
import com.getjavajob.training.web1702.kashapovv.lesson03.Organization.Shareholder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Вадим on 09.03.2017.
 */
public class DomParser implements XmlOrganizationParser {
    private Collection<Organization> orgs;
    private Map<Person, Person> persons;

    @Override
    public Object readXml(InputStream inputStream) throws ParserConfigurationException
            , IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(inputStream);

        orgs = new HashSet();
        persons = new HashMap<>();
        Element orgTag = document.getDocumentElement();
        NodeList nodeList = orgTag.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element && node.getNodeName().equals("organization")) {
                orgs.add(getOrganization(node));
            }
        }
        return orgs;
    }

    private Organization getOrganization(Node node) throws ParentOrgException {
        Organization org = new Organization();

        org.setName(node.getAttributes().getNamedItem("name").getNodeValue());
        NodeList childNodes = node.getChildNodes();
        for (int j = 0; j < childNodes.getLength(); j++) {
            Node childNode = childNodes.item(j);
            String content = childNode.getTextContent().trim();
            switch (childNode.getNodeName()) {
                case "type":
                    org.setType(OrganizationType.valueOf(content));
                    break;
                case "organizationCeo":
                    org.setOrganizationCeo(getPerson(childNode));
                    break;
                case "statedCapital":
                    org.setStatedCapital(Double.parseDouble(content));
                    break;
                case "shareholders":
                    org.setShareholders(getSharers(childNode));
                    break;
                case "parentOrganization":
                    org.setParentOrganization(getOrganization(childNode));
                    break;
                default:
                    break;
            }
        }
        return org;
    }

    private Person checkPersonExist(Person person) {
        if (persons.containsKey(person)) {
            return persons.get(person);
        } else {
            persons.put(person, person);
            return person;
        }
    }

    private List<Shareholder> getSharers(Node node) {
        NodeList childNodes = node.getChildNodes();
        List<Shareholder> sharers = new ArrayList<>();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            if (childNode instanceof Element && childNode.getNodeName().equals("shareholder")) {
                sharers.add(getSharer(childNode));
            }
        }
        return sharers;
    }

    private Shareholder getSharer(Node node) {
        NodeList childNodes = node.getChildNodes();
        Person person = null;
        int shareCount = 0;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = node.getChildNodes().item(i);
            String content = childNode.getTextContent().trim();

            switch (childNode.getNodeName()) {
                case "person":
                    person = getPerson(childNode);
                    break;
                case "sharesProportion":
                    shareCount = Integer.parseInt(content);
                    break;
            }
        }
        return new Shareholder(person, shareCount);
    }

    private Person getPerson(Node node) {
        NodeList childNodes = node.getChildNodes();
        String firstName = null;
        String middleName = null;
        String lastName = null;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = node.getChildNodes().item(i);
            String content = childNode.getTextContent().trim();

            switch (childNode.getNodeName()) {
                case "firstName":
                    firstName = content;
                    break;
                case "middleName":
                    middleName = content;
                    break;
                case "lastName":
                    lastName = content;
                    break;
            }
        }
        return checkPersonExist(new Person(lastName, firstName, middleName));
    }
}