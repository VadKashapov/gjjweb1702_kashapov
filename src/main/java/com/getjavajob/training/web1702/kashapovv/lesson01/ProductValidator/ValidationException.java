package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator;

/**
 * Created by Вадим on 02.03.2017.
 */
public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException() {
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
