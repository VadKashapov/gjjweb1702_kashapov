package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.Product;
import org.junit.Before;
import org.junit.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Вадим on 25.02.2017.
 */
public class NameCheckerTest {
    private NameChecker checker;
    private Product product;

    @Before
    public void setUp() throws Exception {
        checker = new NameChecker();
        product = new Product();
    }

    @Test
    public void validateCorrectProductName() throws Exception {
        product.setProductName("Product1");
        assertEquals(true, checker.validate(product));
    }

    @Test
    public void validateEmptyProductName() throws Exception {
        product.setProductName("");
        assertEquals(false, checker.validate(product));
    }

    @Test
    public void validateNullProductName() throws Exception {
        product.setProductName(null);
        assertEquals(false, checker.validate(product));
    }
}