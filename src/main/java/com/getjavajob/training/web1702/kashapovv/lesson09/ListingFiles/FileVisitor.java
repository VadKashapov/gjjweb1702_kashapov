package com.getjavajob.training.web1702.kashapovv.lesson09.ListingFiles;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;

/**
 * Created by Вадим on 19.03.2017.
 */
public class FileVisitor<E> extends SimpleFileVisitor<E> {
    private PathMatcher matcher;

    public FileVisitor(String fileExt) {
        if (!fileExt.isEmpty()) {
            this.matcher = FileSystems.getDefault().getPathMatcher("glob:*." + fileExt);
        }
    }

    @Override
    public FileVisitResult preVisitDirectory(E dir, BasicFileAttributes attrs) throws IOException {
        if (matcher != null) {
            return FileVisitResult.CONTINUE;
        }
        Path path = (Path) dir;
        StringBuilder builder = new StringBuilder();
        if (attrs.isDirectory()) {
            builder.append('d');
        }
        if (Files.isReadable(path)) {
            builder.append('r');
        }
        if (Files.isWritable(path)) {
            builder.append('w');
        }
        if (Files.isExecutable(path)) {
            builder.append('x');
            builder.append(' ');
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        builder.append(format.format(attrs.lastModifiedTime().toMillis()));
        builder.append(' ').append(path);
        System.out.println(builder);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(E file, BasicFileAttributes attrs) throws IOException {
        Path path = (Path) file;
        if (matcher != null && !matcher.matches(path.getFileName())) {
            return FileVisitResult.CONTINUE;
        }
        StringBuilder builder = new StringBuilder();
        if (Files.isReadable(path)) {
            builder.append('r');
        }
        if (Files.isWritable(path)) {
            builder.append('w');
        }
        if (Files.isExecutable(path)) {
            builder.append('x');
            builder.append(' ');
        }
        if (attrs.isRegularFile()) {
            builder.append(attrs.size() / 1_000.).append("kb ");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        builder.append(format.format(attrs.lastModifiedTime().toMillis()));
        builder.append(' ').append(path);
        System.out.println(builder);
        return FileVisitResult.CONTINUE;
    }
}

