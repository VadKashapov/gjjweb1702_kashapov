package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentAlphabetCounter;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;

import static java.util.Collections.reverseOrder;
import static java.util.Collections.sort;

/**
 * Created by Вадим on 18.03.2017.
 */
public class ConcurrentAlphabetCounter {
    private String text;
    private Map<Character, Integer> letterFrequency;

    public List<Entry<Character, Integer>> cyrillicLetterFrequency(InputStream in, Charset charset) throws FileNotFoundException {
        letterFrequency = new HashMap<>();
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(in, charset))) {
            StringBuilder builder = new StringBuilder();
            while (bufReader.ready()) {
                builder.append(bufReader.readLine());
            }
            this.text = builder.toString().toUpperCase();

            List<Thread> threads = new ArrayList<>();
            threads.add(new LetterReader('Ё', this));
            for (char i = 'А'; i <= 'Я'; i++) {
                threads.add(new LetterReader(i, this));
            }
            for (Thread thread : threads) {
                thread.start();
            }
            for (Thread thread : threads) {
                thread.join();
            }
            List<Entry<Character, Integer>> letterSortByFreq = new ArrayList<>(letterFrequency.entrySet());
            sort(letterSortByFreq, reverseOrder(new Comparator<Entry<Character, Integer>>() {
                @Override
                public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
                    return o1.getValue().compareTo(o2.getValue());
                }
            }));
            return letterSortByFreq;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getText() {
        return text;
    }

    public int getLength() {
        return text.length();
    }

    public Map<Character, Integer> getLetterFrequency() {
        return letterFrequency;
    }
}
