package com.getjavajob.training.web1702.kashapovv.lesson02;

import com.getjavajob.training.web1702.kashapovv.lesson02.Organization.Organization;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Вадим on 04.03.2017.
 */
public class CatalogueReadWriteData {
    public static Set<Organization> readData(File file) {
        Set<Organization> orgs = new HashSet<>();
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            for (; ; ) {
                Object obj = ois.readObject();
                if (obj instanceof Organization) {
                    orgs.add((Organization) obj);
                } else {
                    break;
                }
            }
        } catch (EOFException e) {
            return orgs;
        } catch (Exception e) {
            System.out.println("Catch error while reading data file");
            e.printStackTrace();
        }
        return null;
    }

    public static void writeData(Set<Organization> orgs) {
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream
                (new FileOutputStream(CompaniesCatalogue.FILE_NAME)))) {
            for (Organization org :
                    orgs) {
                out.writeObject(org);
            }
        } catch (IOException e) {
            System.out.println("Error to write file");
            e.printStackTrace();
        }
    }
}