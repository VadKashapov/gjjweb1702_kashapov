package com.getjavajob.training.web1702.kashapovv.lesson02.Organization;

import com.getjavajob.training.web1702.kashapovv.lesson02.Exceptions.*;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 05.03.2017.
 */
public class OrganizationParserTest {
    private Set<Organization> orgs;
    private OrganizationParser parser;

    @Before
    public void setUp() throws Exception {
        orgs = new HashSet<>();
        parser = new OrganizationParser(orgs);
    }

    @Test
    public void parseLineTest() throws Exception {
        Organization org = parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000000 none Sharer1LastName 40 Sharer2LastName 60");
        assertEquals("OrgName", org.getName());
        assertEquals("FirstNameCEO", org.getOrganizationCeo().getFirstName());
        assertEquals("MiddleNameCEO", org.getOrganizationCeo().getMiddleName());
        assertEquals("LastNameCEO", org.getOrganizationCeo().getLastName());
        assertEquals(null, org.getParentOrganization());
        assertEquals("Sharer1LastName", org.getShareholders().get(0).getPerson().getLastName());
        assertEquals(40, org.getShareholders().get(0).getSharesProportion());
        assertEquals("Sharer2LastName", org.getShareholders().get(1).getPerson().getLastName());
        assertEquals(60, org.getShareholders().get(1).getSharesProportion());
    }

    @Test(expected = MoneyValueException.class)
    public void parseLineTest2() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO money none Sharer1LastName 40 Sharer2LastName 60");
    }

    @Test(expected = MoneyValueException.class)
    public void parseLineTest3() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO -1000 none Sharer1LastName 40 Sharer2LastName 60");
    }

    @Test(expected = OrganizationTypeException.class)
    public void parseLineTest4() throws Exception {
        parser.parseLine("OrgName aaa FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000 none Sharer1LastName 40 Sharer2LastName 60");
    }

    @Test(expected = ParentOrgException.class)
    public void parseLineTest5() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000 ParentOrgName Sharer1LastName 40 Sharer2LastName 60");
    }

    @Test(expected = ShareValueException.class)
    public void parseLineTest6() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000 none Sharer1LastName 100 Sharer2LastName 60");
    }

    @Test(expected = ShareValueException.class)
    public void parseLineTest7() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000 none Sharer1LastName 40 Sharer2LastName -60");
    }

    @Test(expected = SharerDataException.class)
    public void parseLineTest8() throws Exception {
        parser.parseLine("OrgName ooo FirstNameCEO MiddleNameCEO " +
                "LastNameCEO 1000 none Sharer1LastName share1Prop Sharer2LastName 60");
    }
}