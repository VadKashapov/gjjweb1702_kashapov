package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerExchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Вадим on 23.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
       Exchanger<Integer> exchangerEvenThread = new Exchanger<>();
       Exchanger<Integer> exchangerOddThread = new Exchanger<>();
        service.execute(new NumConsumer(exchangerEvenThread));
        service.execute(new NumConsumer(exchangerOddThread));
        service.execute(new NumProducer(exchangerEvenThread,exchangerOddThread));
    }
}
