package com.getjavajob.training.web1702.kashapovv.lesson09.PoolThreadAlphabetCounter;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Collections.reverseOrder;
import static java.util.Collections.sort;

/**
 * Created by Вадим on 18.03.2017.
 */
public class PoolThreadAlphabetCounter {
    private static final int RUS_ALPHABET_COUNT = 33;

    private String text;
    private Map<Character, Integer> letterFrequency;
    private CountDownLatch downLatch = new CountDownLatch(RUS_ALPHABET_COUNT);

    public List<Map.Entry<Character, Integer>> cyrillicLetterFrequency(InputStream in, Charset charset)
            throws FileNotFoundException {
        letterFrequency = new ConcurrentHashMap<>();
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(in, charset))) {
            StringBuilder builder = new StringBuilder();
            while (bufReader.ready()) {
                builder.append(bufReader.readLine());
            }
            this.text = builder.toString().toUpperCase();
            ExecutorService poolExecutor = Executors.newCachedThreadPool();
            poolExecutor.submit(new LetterReader('Ё', this));
            for (char i = 'А'; i <= 'Я'; i++) {
                poolExecutor.submit(new LetterReader(i, this));
            }
            downLatch.await();
            List<Map.Entry<Character, Integer>> letterSortByFreq = new ArrayList<>(letterFrequency.entrySet());
            sort(letterSortByFreq, reverseOrder(new Comparator<Map.Entry<Character, Integer>>() {
                @Override
                public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
                    return o1.getValue().compareTo(o2.getValue());
                }
            }));
            return letterSortByFreq;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getText() {
        return text;
    }

    public int getLength() {
        return text.length();
    }

    public Map<Character, Integer> getLetterFrequency() {
        return letterFrequency;
    }

    public CountDownLatch getDownLatch() {
        return downLatch;
    }
}
