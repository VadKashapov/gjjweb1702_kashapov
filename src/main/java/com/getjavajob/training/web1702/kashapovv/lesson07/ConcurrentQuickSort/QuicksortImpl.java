package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort;

/**
 * Created by Вадим on 18.03.2017.
 */
public class QuicksortImpl implements Quicksort {
    public <T extends Comparable<T>> T[] quickSort(T[] elements) {
        int end = elements.length - 1;
        quickSort(elements, 0, end);
        return elements;
    }

    protected <T extends Comparable<T>> void quickSort(T[] numbers, int start, int end) {
        if (start >= end) {
            return;
        }
        int cur = partition(numbers, start, end);
        quickSort(numbers, start, cur);
        quickSort(numbers, cur + 1, end);
    }

    protected <T extends Comparable<T>> int partition(T[] numbers, int start, int end) {
        int i = start;
        int j = end;
        int cur = i - (i - j) / 2;
        while (i < j) {
            while (i < cur && (numbers[i].compareTo(numbers[cur])) <= 0) {
                i++;
            }
            while (j > cur && (numbers[cur].compareTo(numbers[j])) <= 0) {
                j--;
            }
            if (i < j) {
                T temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
                if (i == cur) {
                    cur = j;
                } else if (j == cur) {
                    cur = i;
                }
            }
        }
        return cur;
    }
}
