package com.getjavajob.training.web1702.kashapovv.lesson02;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by Вадим on 05.03.2017.
 */
public class CompaniesCatalogueTest {
    @Test
    public void orgNameWithMaxSubsidiaries() throws Exception {
        CatalogueReadFile reader = new CatalogueReadFile();
        CompaniesCatalogue catalogue = new CompaniesCatalogue();
        catalogue.readFile(new File(reader.getClass().getResource("org.txt").getPath()));
        assertEquals("orgName4", catalogue.orgNameWithMaxSubsidiaries());
    }

    @Test
    public void lastNameRichestSharerAllOrg() throws Exception {
        CatalogueReadFile reader = new CatalogueReadFile();
        CompaniesCatalogue catalogue = new CompaniesCatalogue();
        catalogue.readFile(new File(reader.getClass().getResource("org.txt").getPath()));
        assertEquals("Sharer5LastName", catalogue.lastNameRichestPersonAtAll());
    }
}