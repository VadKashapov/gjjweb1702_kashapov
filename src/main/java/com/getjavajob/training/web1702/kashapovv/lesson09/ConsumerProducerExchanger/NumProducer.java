package com.getjavajob.training.web1702.kashapovv.lesson09.ConsumerProducerExchanger;

import java.util.Random;
import java.util.concurrent.Exchanger;

/**
 * Created by Вадим on 22.03.2017.
 */
public class NumProducer extends Thread implements Runnable {
    private Exchanger<Integer> evenThreadExchanger;
    private Exchanger<Integer> oddThreadExchanger;
    private Random random = new Random();

    public NumProducer(Exchanger<Integer> evenThreadExchanger, Exchanger<Integer> oddThreadExchanger) {
        this.evenThreadExchanger = evenThreadExchanger;
        this.oddThreadExchanger = oddThreadExchanger;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int value = random.nextInt(10);
                if (value % 2 == 0) {
                    evenThreadExchanger.exchange(value);
                } else {
                    oddThreadExchanger.exchange(value);
                }
                sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
