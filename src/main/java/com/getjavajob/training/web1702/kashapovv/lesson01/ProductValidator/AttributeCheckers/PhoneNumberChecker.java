package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class PhoneNumberChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) throws ValidationException {
        if (product.getPhoneNumber().matches("^\\+79[0-9]{9}")) {
            return true;
        } else {
            exceptionMessageBuilder.append("value not match with mobile phone pattern");
        }
        exceptionMessageBuilder.insert(0,"Field: phoneNumber");
        throw new ValidationException(exceptionMessageBuilder.append(", ").append("value = ")
                .append(product.getPhoneNumber()).append(" expected pattern: +79[0-9]... with length: 11").toString());
    }
}
