package com.getjavajob.training.web1702.kashapovv.lesson09.PoolThreadAlphabetCounter;

/**
 * Created by Вадим on 18.03.2017.
 */
public class LetterReader implements Runnable {
    PoolThreadAlphabetCounter counter;
    int count;
    char countedChar;

    public LetterReader(char ch, PoolThreadAlphabetCounter counter) {
        this.countedChar = ch;
        this.counter = counter;
    }

    @Override
    public void run() {
        String text = counter.getText().toUpperCase();
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            if (countedChar == ch) {
                count++;
            }
        }
        counter.getLetterFrequency().put(countedChar, count);
        counter.getDownLatch().countDown();
    }
}
