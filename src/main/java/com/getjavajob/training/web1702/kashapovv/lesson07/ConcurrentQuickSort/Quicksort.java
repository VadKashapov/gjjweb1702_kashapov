package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort;

/**
 * Created by Вадим on 27.03.2017.
 */
public interface Quicksort {
    <T extends Comparable<T>> T[] quickSort(T[] numbers);
}
