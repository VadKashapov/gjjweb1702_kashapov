package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers.AbstractProductAttributeChecker;

/**
 * Created by Вадим on 23.02.2017.
 */
public class Validator {
    private AbstractProductAttributeChecker[] checkers;

    public Validator(AbstractProductAttributeChecker[] checkers) {
        if (checkers == null) {
            throw new NullPointerException("Checkers not exist");
        }
        this.checkers = checkers;
    }

    public void setCheckers(AbstractProductAttributeChecker[] checkers) {
        this.checkers = checkers;
    }

    public void validate(Product product) throws ValidationException {
        try {
            for (AbstractProductAttributeChecker checker : checkers) {
                checker.validate(product);
            }
        } catch (ValidationException e) {
            throw new ValidationException("Validation Failed for product order:\n" + product.toString(),e);
        }
    }
}
