package com.getjavajob.training.web1702.kashapovv.Utils;

import java.util.Scanner;

/**
 * Created by Вадим on 22.02.2017.
 */
public class ConsoleIO {
    private static Scanner input = new Scanner(System.in);

    public static int readInt() {
        return input.nextInt();
    }

    public static double readDouble() {
        return input.nextDouble();
    }

    public static String readString() {
        return input.next();
    }
}
