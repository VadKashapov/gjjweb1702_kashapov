package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

import java.util.Date;

/**
 * Created by Вадим on 23.02.2017.
 */
public class ShippingDateChecker extends AbstractProductAttributeChecker {

    @Override
    public boolean validate(Product product) throws ValidationException {
        if (product.getShippingDate().after(new Date())) {
            return true;
        } else {
            exceptionMessageBuilder.insert(0,"Field: shippingDate").append("Date not after today.")
                    .append(" Value= ").append(product.getShippingDate());
        }
        throw new ValidationException(exceptionMessageBuilder.toString());
    }
}
