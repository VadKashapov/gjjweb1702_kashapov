package com.getjavajob.training.web1702.kashapovv.lesson07.ConcurrentQuickSort;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Вадим on 18.03.2017.
 */
public class QuicksortImplTest {
    private Quicksort quicksort;

    @Test
    public void quickSort() throws Exception {
        Random random = new Random(200);
        Integer[] numbers = new Integer[100];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }
        Integer[] copy = Arrays.copyOf(numbers,numbers.length);
        Arrays.sort(copy);
        assertArrayEquals(copy, new QuicksortImpl().quickSort(numbers));
    }

    @Test
    public void concurrentQuickSort() throws Exception {
        Random random = new Random(200);
        Integer[] numbers = new Integer[100];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }
        Integer[] copy = Arrays.copyOf(numbers,numbers.length);
        Arrays.sort(copy);
        assertArrayEquals(copy, new ConcurrentQuicksort().quickSort(numbers));
    }

    @Test
    public void quickSortComp() throws Exception {
        Random random = new Random(200);
        Integer[] numbers = new Integer[100_000];
        quicksort = new QuicksortImpl();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }
        Integer[] copy = Arrays.copyOf(numbers,numbers.length);

        long start = currentTimeMillis();
        quicksort.quickSort(numbers);
        long end = currentTimeMillis() - start;

        quicksort = new ConcurrentQuicksort();

        start = currentTimeMillis();
        quicksort.quickSort(copy);
        long end2 = currentTimeMillis() - start;

        System.out.println("SingleThread: " + end + " ms");
        System.out.println("ManyThread: " + end2 + " ms");
    }
}