package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Circle extends AbstractFigureBase {
    private double radius;

    public Circle(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be positive number");
        }
        this.radius = radius;
    }

    @Override
    public double getBaseArea() {
        return PI * pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * PI * radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public int hashCode() {
        return Double.valueOf(radius).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Circle cObj = ((Circle) obj);
        return this.getRadius() == cObj.getRadius();
    }

    @Override
    public String toString() {
        return "Circle radius: " + radius;
    }
}
