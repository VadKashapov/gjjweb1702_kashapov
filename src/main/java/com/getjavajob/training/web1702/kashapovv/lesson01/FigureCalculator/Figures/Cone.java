package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.Figures;

import com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases.Circle;

import static java.lang.Math.*;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Cone extends AbstractObligeFigure {

    public Cone(double baseRadius, double height) {
        super(new Circle(baseRadius), height);
    }

    @Override
    public double getHorizontalCrossSectionArea(double crossSectionHeight) {
        if (crossSectionHeight < 0) {
            throw new IllegalArgumentException("Cross Section Height must be positive number");
        }
        Circle baseCircle = (Circle) figureBase;
        double radius = baseCircle.getRadius();
        double height = getHeight();
        return new Circle(radius * (height - crossSectionHeight) / (2 * height)).getBaseArea();
    }

    @Override
    public double getSurfaceArea() {
        Circle baseCircle = (Circle) figureBase;
        double radius = baseCircle.getRadius();
        return PI * radius * sqrt(pow(getHeight(), 2) + pow(radius, 2)) + getBaseArea();
    }
}
