package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.AttributeCheckers;

import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.Product;
import com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator.ValidationException;

/**
 * Created by Вадим on 23.02.2017.
 */
public class PriceChecker extends AbstractProductAttributeChecker {
    @Override
    public boolean validate(Product product) throws ValidationException {
        if (isNatural(product.getProductPrice())) {
            return true;
        }
        exceptionMessageBuilder.insert(0,"Field: productPrice");
        throw new ValidationException(exceptionMessageBuilder.append(" must be natural number").toString());
    }
}
