package com.getjavajob.training.web1702.kashapovv.lesson01.ProductValidator;

import java.lang.reflect.Field;

/**
 * Created by Вадим on 01.03.2017.
 */
public class FieldDescriptionPrinter {
    public static void main(String[] args) {
        Product product = new Product("Smetana", 5, 30, 55.00, 0
                , "A001", "MailExapmle1999@mail.ru", "+79850000000", "27.02.2017");
        print(product);
    }

    public static void print(Product product) {
        for (Field field : product.getClass().getDeclaredFields()) {
            try {
                System.out.printf("Field %s - %s%n", field.getName()
                        , field.getAnnotation(FieldDescription.class).value());
            } catch (NullPointerException e) {
                //Field not annotated
            }
        }
    }
}
