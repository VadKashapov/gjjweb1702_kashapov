package com.getjavajob.training.web1702.kashapovv.lesson01.FigureCalculator.FigureBases;

import static java.lang.Math.pow;

/**
 * Created by Вадим on 22.02.2017.
 */
public class Square extends Polygon {
    private static final int CORNER_NUMBER = 4;

    public Square(double edgeLength) {
        super(CORNER_NUMBER, edgeLength);
    }

    @Override
    public double getBaseArea() {
        return pow(getEdgeLength(), 2);
    }

    @Override
    public double getInCircleRadius() {
        return getEdgeLength() / 2.0;
    }

    @Override
    public String toString() {
        return "Square has " + getEdgeLength() + " edge length";
    }
}
