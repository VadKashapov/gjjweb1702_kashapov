package com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator;

import com.getjavajob.training.web1702.kashapovv.lesson00.ProductValidator.AttributeCheckers.*;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Вадим on 24.02.2017.
 */
public class ValidatorTest {
    private Validator validator;

    @Test
    public void validateIncorrectOrderWithWrongPhoneNumber() throws Exception {
        validator.setCheckers(new AbstractProductAttributeChecker[]{new NameChecker()
                , new QuantityRequiredChecker()
                , new PriceChecker(), new SaleSizeChecker(), new ClientIdChecker(), new EmailChecker()
                , new PhoneNumberChecker(), new ShippingDateChecker()});
        assertTrue(validator.validate(new Product("Smetana", 5, 30, 55.00, 0
                , "A001", "MailExapmle1999@mail.ru", "+7985000000d", "27.02.2017")));
    }

    @Test
    public void validateCorrectOrder() throws Exception {
        validator = new Validator(new AbstractProductAttributeChecker[]{new NameChecker()
                , new QuantityRequiredChecker()
                , new PriceChecker(), new SaleSizeChecker(), new ClientIdChecker(), new EmailChecker()
                , new PhoneNumberChecker(), new ShippingDateChecker()});
        assertTrue(validator.validate(new Product("Smetana", 5, 30, 55.00, 0
                , "A001", "MailExapmle1999@mail.ru", "+79850000000", "27.02.2017")));
    }
}